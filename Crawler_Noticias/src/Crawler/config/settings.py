# -*- coding:utf-8 -*-


# DICTIONARY_* son archivos que contienen las palabras mas frecuentes de un idioma
DICTIONARY_SPANISH = "../resource/dictionary_spanish.txt"
DICTIONARY_ENGLISH = "../resource/dictionary_english.txt"

# LEMATIZER_* son archivos que contienen los lemas
LEMATIZER_SPANISH = "../resource/lematizer_spanish.txt"
LEMATIZER_ENGLISH = "../resource/lematizer_english.txt"

# STOPWORDS_* son archivos que contienen los stopwords
STOPWORDS_SPANISH = "../resource/stopwords_spanish.txt"
STOPWORDS_SPANISH_OPINION_MINING = "../resource/stopwords_spanish_opinion_mining.txt"
STOPWORDS_ENGLISH = "../resource/stopwords_english_big.txt"

# Recursos Lexicos para el analisis de sentimiento
EMOTICONS = "../resource/dictionary/emoticons.txt"
BOOSTER_WORDS_SPANISH = "../resource/dictionary/booster_words_spanish.txt"
SENTIMENT_WORDS_SPANISH = "../resource/dictionary/sentiment_words_spanish.txt"
SLANGS_PERUVIAN = "../resource/dictionary/slangs_peruvian.txt"
NEGATING_WORDS_SPANISH = "../resource/dictionary/negating_words_spanish.txt"
COMBINATIONS_SPANISH  = "../resource/dictionary/combinations_spanish.txt"
COMBINATIONS_SLANGS_PERUVIAN = "../resource/dictionary/combinations_slangs_peruvian.txt"
PUNCTUATION = "../resource/dictionary/punctuation.txt"

# Tipos de Terminos
TERM_TYPE_EMOTICON = 'emoticon'
TERM_TYPE_BOOSTER = 'booster'
TERM_TYPE_WORD_SLANG = 'word_slang'
TERM_TYPE_COMBINATION = 'combination'
TERM_TYPE_NEGATING = 'negating'
TERM_TYPE_PUNCTUATION = 'punctuation'
TERM_TYPE_NEUTRO = 'neutro'

# Simbolos adicionales
FLEXIS_SIMBOL = '#'
SPLITTER_WEIGHTS = '=='
ENCODING = 'utf-8'
TERM_NOT_FOUND = ''

''' <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Package Crawler >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> '''

## Datos de la coneccion a la base de datos.
ENGINE =  '{SQL Server Native Client 11.0}'
#ENGINE = 'FreeTDS'

'''version de TDS a utilizar (linux)'''
#TDS_VERSION = '8.0'

'''Nombre de la base de datos'''
# DATABASE = 'BD_Lindexa_Amazon_Final'#'roque'
DATABASE = 'Prueba'

# Host donde esta alojada la base de datos ('localhost' si es una base de datos local).
#HOST = 'ITSERVER\SQL2008R2'#'PPSERVER\SQLSERVER2008'
HOST = '192.168.101.164,49172' 

#Puerto para conexión
PORT= '1433'

# username para conectarse con la base de datos.
USER = 'sa'
# password del username, usado en la conexion con la base de datos.
#PASSWORD = 'ladrillo00.'
PASSWORD = 'research'
''' <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Package Crawler Facebook >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> '''

# Tipos de posts
TYPE_COMMENT = 0
TYPE_PUBLICATION = 1

''' <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Package Crawler Noticias >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> '''

# Intervalo de tiempo para sincronizar los nuevos requests (en segundos).
SYNC_INTERVAL = 15 * 60

# Intervalo de tiempo para actualizar los datos "dinamicos" de las noticias (en segundos).
UPDATE_INTERVAL = 15 * 60

# Tiempo que una thread permanece en sleep (en segundos).
CHECKING_TIME = 15

# Numero de intentos maximo para obtener datos de redes sociales (facebook, twitter, google+)
MAX_ATTEMPTS = 1

# Ruta absoluta (absolute path) donde quiere que se almacenen los archivos con los textos de las noticias
#NEWS_FILES_PATH = r'D:\DOCUMENTS\Sweb\lindexa\crawler_noticias\programa\noticias'
NEWS_FILES_PATH = r'../output/Noticias'

# Ruta absoluta (absolute path) donde quiere que se almacenen los logs
LOG_PATH = r'../output/Logs'

# Nombre del archivo donde se van a almacenar los logs
LOG_FILENAME = 'LogsNoticias.log'

# Dependiendo de que logs quiere ver. Opciones: 'DEBUG', 'INFO', 'WARNING',
#                                               'ERROR', 'CRITICAL'.
LOG_LEVEL = 'INFO'

MARCA = 1
COMPETENCIA = 2
SINONIMO = 3