# -*- coding:utf-8 -*-


from Crawler.config.utils import normalize_string
from Crawler.config import database as db

class InsertRelationship(object):

    def __init__(self, id_source, ds_source):
        self.__id_source = id_source
        self.__source = ds_source
        self.__tupla_relkeyword_grupo_lema = None
        self.__alert_list = None
                                
    def insert_todo_by_news(self, id_news, tittle, url, id_section, tag_list):
        print(self.__tupla_relkeyword_grupo_lema)
        for tag in tag_list:
            #print(tag)
            db.stored_procedure(db.STP_INS_TODO_BY_NEWS, id_news, tag, self.__tupla_relkeyword_grupo_lema)
            #print(tag, ' terminado')
    
    def search(self, comment, keyword):
        comment = normalize_string(comment.lower())
        keyword = keyword.lower()
        keyword_list = keyword.split(" ")
        for key in keyword_list:
            if comment.count(key) < 1:
                return False              
        return True         
            
    def update(self):
        text_list = []
        for costumer in db.stored_procedure(db.STP_SEL_CLIENTES):
            text_list += self.__create_tupla(costumer.id_Cliente)
        if len(text_list) > 0:
            print(text_list)
            self.__tupla_relkeyword_grupo_lema = ";".join(text_list)      
    
    def __create_tupla(self, id_customer):
        text_list = []
        for synonymous in db.stored_procedure(db.STP_SEL_TUPLA_BY_SYNONYMOUS, id_customer):
            if synonymous.ds_Lema != None:
                text_list.append(str(synonymous.id_RelKeyword)+','+synonymous.ds_Lema)
                for row in db.stored_procedure(db.STP_SEL_TUPLA_BY_PAGE, synonymous.id_grupo):
                    if row.id_RelKeyword != None and row.ds_Lema != None:
                        text_list.append(str(row.id_RelKeyword)+','+row.ds_Lema+' '+synonymous.ds_Lema)
        return text_list
      
                                                              
if __name__ == '__main__':
    obj = InsertRelationship(15,'Gestion.pe')
    obj.update()
    obj.insert_todo_by_news(2985,'','', 147,['ministros','peru','presidente'])
    print('ll')