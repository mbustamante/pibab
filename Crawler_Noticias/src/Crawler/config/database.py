#-*- coding:utf-8 -*-

import pyodbc  # @UnresolvedImport
from datetime import datetime, date
from Crawler.config.settings import ENGINE, USER, PASSWORD, HOST, PORT, DATABASE
from Crawler.config.utils import normalize_string_sp, write_log

STP_ALMACENAR_NOTICIA = 'stp_ins_Noticia'
STP_ACTUALIZAR_NOTICIA = 'stp_upd_Noticia'
STP_ALMACENAR_COMENTARIO = 'stp_ins_ComentarioNoticia'
STP_ALMACENAR_REL_KEYWORD_NOTICIA = 'stp_ins_RelKeywordNoticia'
STP_FILTRAR_FEEDS_REQ = 'stp_sel_RequestNoticia'
STP_FILTRAR_NOTICIAS = 'stp_sel_NoticiasXActualizar'
STP_ALMACENAR_CTRL_FEED = 'stp_ins_ControlNoticia'

#Roque
STP_INS_KEYWORD = 'stp_ins_Keyword'
STP_INS_REL_KEY_SUB = 'stp_ins_RelKeySubkey'
STP_INS_REL_KEY_LEMA = 'stp_ins_RelLemaKey'
STP_INS_LEMA = 'stp_ins_Lema'
STP_SEL_IDKEYWORD_BY_LEMA = 'stp_sel_IdKeywordByLema'
STP_EXISTS_LEMA = 'stp_sel_IdLema'
STP_SEL_KEYWORD = 'stp_sel_Keyword'
STP_SEL_NOTICIA_SIN_SENTIMIENTO = 'stp_sel_NoticiaSinSentimiento'
STP_UPD_SENTIMIENTO_NOTICIA = 'stp_upd_SentimientoNoticia'
#---
STP_INS_TODO_BY_NEWS = 'stp_ins_BusquedaXNoticia'
STP_INS_TODO_BY_KEYWORD = 'stp_ins_TodoByKeyword'
STP_INS_TODO_BY_PAGE = 'stp_ins_TodoByPage'
STP_SEL_CLIENTE_BY_KEYWORD ='stp_sel_ClienteByIdRelKeyword'
STP_SEL_TUPLA = 'stp_sel_TuplaRelKeywordGrupoLema'
STP_SEL_CLIENTE_BY_PAGE ='stp_sel_ClienteByIdRelPagina'
STP_SEL_TUPLA_BY_PAGE = 'stp_sel_TuplaXPagina'
STP_SEL_TUPLA_BY_SYNONYMOUS = 'stp_sel_TuplaXSinonimo'
STP_SEL_CLIENTES ='stp_sel_Clientes'
STP_SEL_ALERT_BY_NEWS = 'stp_sel_AlertaXNoticias'
STP_INS_REL_ALERTA_NOTICIA = 'stp_ins_RelAlertaNoticia'
STP_SEL_ALERTS_KEYWORD ='stp_sel_KeywordsDeAlertas'
STP_SEL_USERS_ALERT = 'stp_sel_UsuariosAlertas'
STP_SEL_RESUMEN_CUADRO = 'stp_sel_ResumenTabla'
STP_SEL_RESUMEN_MAESTRO = 'stp_sel_NoticiaMaestro'
STP_SEL_RESUMEN_DETALLE = 'stp_resumen_noticia'


def parse_param(param):
    """ Coloca formato a los parametros de las querys segun su tipo """
    if param is None:
        return 'NULL'
    if isinstance(param, str):
        return "'%s'" % param.replace('\'','\'\'')
    if isinstance(param, datetime):
        new_param = param.replace(microsecond=0)
        return "'%s'" % new_param
    if isinstance(param, date):
        return "'%s'" % param
    return str(param)

def stored_procedure(cmd, *params):
    cnxn = pyodbc.connect('DRIVER=%s;SERVER=%s;PORT=%s;DATABASE=%s;UID=%s;PWD=%s;Trusted_Connection=no'
                      % (ENGINE, HOST,PORT, DATABASE, USER, PASSWORD))

    cursor = cnxn.cursor()
        
    cmd_params = '(%s)' % ', '.join(['%s'] * len(params))
    final_cmd = 'call ' + cmd + cmd_params % tuple(parse_param(param) for param in params)
    write_log('debug', 'Ejecutando stored procedure "%s"', final_cmd)
    try:
        cursor.execute('{'+final_cmd+'}')
        result = cursor.fetchall()
    except Exception as e:
        print(e)
        write_log('warning', 'Fallo en la codificacion al ejecutar el stored_procedure "%s", probando con "%s"', final_cmd, normalize_string_sp(final_cmd))
        cursor.execute('{'+normalize_string_sp(final_cmd)+'}')
        result = cursor.fetchall()
    cnxn.commit()
    return result

if __name__ == '__main__':
    tupla = '99,autos;100,seguro;101,seguros;102,autos;56,claro;98,rpc claro;104,Educación;105,educacion;106,verano;107,peru;108,Haya;109,ONG;110,chavin;40,ClaroTv;90,caimancitto;91,caimaw;93,caimawh;94,caimawht;95,caimawhtj;96,caimann;97,caimannn'
    a = stored_procedure(STP_INS_TODO_BY_NEWS,2985,'presidente',tupla)
    print(a)

