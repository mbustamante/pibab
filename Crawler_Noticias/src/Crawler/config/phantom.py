#-*- coding:utf-8 -*-

# import os
import os.path
import hashlib
import requests
import subprocess
import codecs
from bs4 import BeautifulSoup, Comment
from selenium import webdriver


PHANTOM_DIR = os.path.join(os.getcwd(), 'phantom')
try:
    os.stat(PHANTOM_DIR)
except OSError:
    os.mkdir(PHANTOM_DIR)
PHANTOM_TEMPLATE = """var page = require('webpage').create();  
page.open('%(url)s', function (status) {
    if (status !== 'success') {
        console.log('Unable to access network');
    } else {
        var p = page.evaluate(function () {
            return document.getElementsByTagName('html')[0].innerHTML
        });
        console.log(p);
    }
    phantom.exit();
});"""

def escape_quotes(text):
    return text.replace('"', '\\"').replace("'", "\\'")

def remove_unncesary_tags(soup):
    # Remove <script>
    for script in soup("script"):
        soup.script.extract()
    # Remove <style>
    for style in soup("style"):
        soup.style.extract()
    # Remove HTML comments
    for comment in soup.findAll(text=lambda x: isinstance(x, Comment)):
        comment.extract()
    return soup

def get_soup(url, clean=True):
    html = requests.get(url)
    soup = BeautifulSoup(html.text)
    return remove_unncesary_tags(soup) if clean else soup    

def get_executed_soup(url, file_id=None, clean=True):
    
#===============================================================================
#     file_id = file_id or hashlib.md5(url.encode('utf8')).hexdigest()
#     PHANTOM_ABS_PATH = os.path.join(PHANTOM_DIR, 'phantom%s.js' % file_id)
#     OUTPUT_ABS_PATH = os.path.join(PHANTOM_DIR, 'output%s.html' % file_id)
#     phantom = codecs.open(PHANTOM_ABS_PATH, 'w', encoding='utf8')
#     phantom.write(PHANTOM_TEMPLATE % {'url': escape_quotes(url)})
#     phantom.close()
#     cmd = 'phantomjs ' + PHANTOM_ABS_PATH + ' > ' + OUTPUT_ABS_PATH
#     stdout, stderr = subprocess.Popen(cmd, shell=True).communicate()
#     output = codecs.open(OUTPUT_ABS_PATH, 'r', encoding='utf8')
# 
#     soup = BeautifulSoup(output.read())
#     output.close()
#     os.remove(PHANTOM_ABS_PATH)
#     os.remove(OUTPUT_ABS_PATH)
#===============================================================================
    browser = webdriver.Chrome()
    browser.set_page_load_timeout(40)
    
    #browser.get("http://www.google.com.pe")
    
    try:
        browser.get(url)
        browser.refresh()
        #browser.back()
        #browser.forward()
        #browser.refresh()
        soup = BeautifulSoup(browser.page_source)
        browser.close()
        browser.quit()
    except:
        soup = BeautifulSoup()
        browser.quit()
    
    
    
    return remove_unncesary_tags(soup) if clean else soup

if __name__ == '__main__':  
    #get_soup('http://diariocorreo.pe/ultimas/noticias/8125032/miscelanea/empleado-del-ano-premiado-con-noche-junto-a')
    ss=get_executed_soup('http://disqus.com/embed/comments/?disqus_version=77ed3016&base=default&f=diariocorreo&t_u=http%3A%2F%2Fdiariocorreo.pe%2Fultimas%2Fnoticias%2F8562406%2Fhoy-se-celebra-el-dia-internacional-del-gato&t_d=Hoy%20se%20celebra%20el%20D%C3%ADa%20Internacional%20del%20Gato&t_t=Hoy%20se%20celebra%20el%20D%C3%ADa%20Internacional%20del%20Gato&s_o=default#2')
    print(ss)
    print('hola mundo')