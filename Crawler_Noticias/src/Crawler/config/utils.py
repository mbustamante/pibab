#-*- coding:utf-8 -*-

import os
import logging
from logging import handlers
import re
from time import mktime
from datetime import datetime, timedelta
from unicodedata import normalize
from HTMLParser import HTMLParser
from Crawler.config.settings import LOG_PATH, LOG_FILENAME, LOG_LEVEL
from Crawler.config.phantom import get_executed_soup

MAIN_LOGGER = logging.getLogger('main_logger')

def configure_logging():
    try:
        os.stat(LOG_PATH)
    except OSError:
        os.mkdir(LOG_PATH)
    log_file = os.path.join(LOG_PATH, LOG_FILENAME)
    logging.basicConfig(level=getattr(logging, LOG_LEVEL), format='%(message)s')
    handler = handlers.TimedRotatingFileHandler(log_file, 'midnight', 1)
    handler.suffix = '%d-%m-%Y'
    MAIN_LOGGER.addHandler(handler)
    logging.getLogger('sqlalchemy.engine').setLevel(logging.ERROR)

def write_log(level, message, *args):
    """ Escribir un mensaje en el log, especificando su nivel """
    try:
        getattr(MAIN_LOGGER, level)(
                '[{0:<8}] [{1:%d/%m/%Y %H:%M:%S}] {2}'.format(level.upper(),
                                                               datetime.now(),
                                                              message % args))
    except Exception as e:
        print(e)
         
def normalize_string(string):
    """ Reemplazar caracteres especiales unicode por caracteres ascii """
    return str(normalize('NFKD', string).encode('ascii', 'ignore'), 'utf-8')

def normalize_string_sp(string):
    """ Elimina caracteres especiales no admitidos al llamar a un procedimiento almacenado"""
    string = string.replace('{', ' ')
    string = string.replace('}', ' ')
    string = string.replace('"', ' ')
    string = string.replace('\\', ' ')
    string = string.replace('$', ' ')
    return normalize_string(string)

def get_cleaned_text(soup):
    return ' '.join(elem for elem in soup.recursiveChildGenerator()
                                                if isinstance(elem, str))

def remove_punctuation_marks(word):
    if re.match("^[a-z0-9\xE1\xE9\xED\xF3\xFA\xF1]+$", word):
        return word
    else:
        new_word = ""
        for i in range(len(word)):
            if re.match("[\w\xE1\xE9\xED\xF3\xFA\xF1]", word[i]):
                new_word += word[i]
        return new_word
    
def remove_whitespaces(text):
    cleaned_text = re.sub('\s+', ' ', text)
    return cleaned_text.strip()

def unescape_html(text):
    return HTMLParser.unescape(HTMLParser, text)

def clean_soup(soup):
    return remove_whitespaces(unescape_html(get_cleaned_text(soup)))

def get_news_datetime(my_date, hour):
    """ Transformar fecha de creacion"""
    real_datetime = my_date + timedelta(hours=hour)
    real_datetime = real_datetime.strftime("%Y-%d-%m %H:%M:%S")
    return real_datetime

def parse_entry_date(entry):
    news_dt = datetime.fromtimestamp(mktime(entry.published_parsed))
#    match = re.match('[+|-]\d{4}', entry.published[-5:])
#    if match:
#        news_dt += timedelta(hours=int(match.group())/100.0)
    return news_dt

def parse_news_syntax(specs_str):
    """ tag[?attribute=value[,...][?(position|*)[?default]]][|...] """
    opt = []
#     print(specs_str.split('|'))
    for option in specs_str.split('#'):
        specs = []
        for elem in option.split('|'):
            values = elem.split('?')
#             print(values)
            spec = {'tag': values[0]}
            try:
                spec['attrs'] = dict(attr.split('=')
                                        for attr in values[1].split(',') if attr)
            except IndexError:
                spec['attrs'] = {}
            try:
                spec['pos'] = '*' if values[2] == '*' else int(values[2])
            except IndexError:
                spec['pos'] = 0
            try:
                spec['default'] = values[3]
            except IndexError:
                spec['default'] = None
            specs.append(spec)
        opt.append(specs)
#         print(spec)
#     print(specs)
    return opt

def spec_to_content(soup, specs_str, single_elem=True):
    if not specs_str:
        return None if single_elem else []
    content = soup
#     print(content,'\n\n\n')
    options = parse_news_syntax(specs_str)
    for specs in options:
        for spec in specs:
            content = content.findAll(spec['tag'], spec['attrs'])
            if spec['pos'] != '*':
                try:
                    content = content[spec['pos']]
                except IndexError:
                    return spec['default'] or None if single_elem else []
#             print('\n\n\n',content)
            if spec['tag'] == 'iframe' and type(content) is not list:
                #a = content['src']
                content = get_executed_soup(content['src'])
                #print('ejecutado: ',a,'\n',content)
        if isinstance(content, list):
            content = [clean_soup(elem) for elem in content]
            if single_elem:
                content = ' '.join(content)
        else:
            content = clean_soup(content)
        if content != '':
            break
    if content == '':
        return None
    return content

numerize = lambda u: int(u) if u and u.isdigit() else None

