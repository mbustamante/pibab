#-*- coding:utf-8 -*-

import os
import codecs
import hashlib

from Crawler.config.settings import NEWS_FILES_PATH
from Crawler.config.utils import numerize, normalize_string_sp, write_log, get_news_datetime
from Crawler.config import database as db

try:
    os.stat(NEWS_FILES_PATH)
except OSError:
    os.mkdir(NEWS_FILES_PATH)

def create_noticia(news, classifier, obj_insert, hour_diff, is_normal=True, id_fuente=None):
    """ Almacenar una noticia en la base de datos. """
    news_filename = os.path.join(NEWS_FILES_PATH, '%s.txt' % news.hex_id)
    news_file = codecs.open(news_filename, 'w', encoding='utf8')
    news_file.write(news.text)
    news_file.close()
    title = news.title
    summary = news.summary  
    write_log('debug', 'Almacenando la noticia "%s" en la base de datos', title)
    print('insertando noticia: ',title)
    
    classifier.classify(news.text, '1')    
    row = db.stored_procedure(db.STP_ALMACENAR_NOTICIA,
                              news_filename,
                              title,
                              summary,
                              normalize_string_sp(" ".join(news.tags)),
                              news.category,
                              get_news_datetime(news.published, hour_diff),
                              news.url,
                              id_fuente,
                              news.hex_id,
                              is_normal,
                              classifier.get_contribution_positive(),
                              classifier.get_contribution_negative(),
                              classifier.get_score())[0]
    id_noticia, id_seccion, created = row.id_noticia, row.id_seccion, int(row.yn_nuevo)
    if created:
        print('created')
        text_list = news.tags
        if title: text_list.append(title)
        if summary: text_list.append(summary)
        obj_insert.insert_todo_by_news(id_noticia, title, news.url, id_seccion, text_list)
        print("relación insertada")
    return created

def update_noticia(classifier, news, news_data):
    """ Actualizar los datos dinamicos de una noticia, es decir, los datos
        que varian: comentarios e informacion de redes sociales (facebook,
        twitter, google+) """
    write_log('debug', 'Actualizando los datos de la noticia con id_noticia=%s en la base de datos', news.id_noticia) 
    print("TW", news_data['twitter_count'], "FB", news_data['facebook_count'], "GP", news_data['gplus_count'], news.url,'\n')
    db.stored_procedure(db.STP_ACTUALIZAR_NOTICIA,
                        news.id_noticia,
                        numerize(news_data['facebook_count']),
                        numerize(news_data['twitter_count']),
                        numerize(news_data['gplus_count']))
    for comment in news_data['comments']:
        print(comment.encode('utf8'))
        hex_id = hashlib.md5(comment.encode('utf8')).hexdigest()
        classifier.classify(comment, '1')
        db.stored_procedure(db.STP_ALMACENAR_COMENTARIO,
                            hex_id, 
                            comment, 
                            news.id_noticia,
                            classifier.get_contribution_positive(),
                            classifier.get_contribution_negative(),
                            classifier.get_score())

