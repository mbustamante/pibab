#-*- coding:utf-8 -*-

import feedparser
import hashlib

from Crawler.config.phantom import get_soup, get_executed_soup
from Crawler.config.utils import parse_entry_date, spec_to_content
from Crawler.config.settings import MAX_ATTEMPTS
from datetime import datetime

class News(object):
    def __init__(self, **kwargs):
        self.url = kwargs.get('url')
        self.hex_id = kwargs.get('hex_id')
        self.category = kwargs.get('category')
        self.title = kwargs.get('title')
        self.published = kwargs.get('published')
        self.summary = kwargs.get('summary')
        self.text = kwargs.get('text')
        self.tags = kwargs.get('tags')

class NewsFeed(object):
    def __init__(self, feed_link, **specs):
        print(feed_link,specs)
        self.feed_link = feed_link
        self.specs = specs

    def get_news(self, since_id=None):
        feed = feedparser.parse(self.feed_link)
        all_news = []
#         print(feed)

        for entry in feed.entries:
            try:
                #print('\n',entry)
                print('\n',entry.link)
                print(entry.title)
                #print(parse_entry_date(entry))
                hex_id = hashlib.md5(entry.link.encode('utf8')).hexdigest()
                if hex_id == since_id:
                    break
                news = News(url=entry.link, title=entry.title, hex_id=hex_id)
                news.published = parse_entry_date(entry)
                soup = get_soup(entry.link)
    #             print(soup)
                news.category = spec_to_content(soup, self.specs['category'])
                #print(news.category)
                news.summary = spec_to_content(soup, self.specs['summary'])
                #print(news.summary  )
                news.text = spec_to_content(soup, self.specs['text'])
                #print(news.text)
                news.tags = spec_to_content(soup, self.specs['tags'], False)
                #print(news.tags)
                if news.text:
                    all_news.append(news)
            except Exception as e:
                print (e, ' -> error inesperado, noticia no recogida')
                
        return all_news

def get_news_dynamic_data(url, hex_id=None, **specs):
    soup = None
    
    print(datetime.today(),'  Obteniendo data dinámico, ')
    facebook_count = None
    twitter_count = None
    gplus_count = None
    for i in range(MAX_ATTEMPTS):
        if facebook_count!=None and twitter_count!=None and gplus_count!=None:
            break
        soup = get_executed_soup(url)
        #print(soup)
        if facebook_count==None and specs['facebook_count']:
            facebook_count = spec_to_content(soup, specs['facebook_count'])
        if twitter_count==None and specs['twitter_count']:
            twitter_count = spec_to_content(soup, specs['twitter_count'])
        if gplus_count==None and specs['gplus_count']:
            gplus_count = spec_to_content(soup, specs['gplus_count'])
            
    soup = soup or get_executed_soup(url, hex_id)    
    comments = spec_to_content(soup, specs['comments'], False)
    print(datetime.today(),'  retornando data dinamico')
    result = {'comments': comments, 'facebook_count': facebook_count,
            'twitter_count': twitter_count, 'gplus_count': gplus_count}
    print(result)
    return result

if __name__ == '__main__':
    url = 'http://diariocorreo.pe/RSS-portlet/feed/correo/todas-las-secciones'
    news = NewsFeed(url,
                    category='li?class=n2',
                    summary='',
                    text='div?class=text colorBlack',
                    tags='')
    all_news = news.get_news()
    print('Total noticias: ',len(all_news ),'\n')
    for n in all_news:
        print(n.url)
        get_news_dynamic_data(n.url, n.hex_id ,
                                    comments='iframe?id=dsq-2|div?id=postCompatContainer|p??*',
                                    facebook_count='iframe?title=fb:like Facebook Social Plugin|span?class=pluginCountTextDisconnected',
                                    twitter_count='iframe?id=twitter-widget-0|div?id=c|a?id=count',
                                    gplus_count='iframe?title=+1|div?id=aggregateCount')
