#-*- coding:utf-8 -*-

import sys
import threading
from Crawler.config import database as db
from time import time, sleep
from datetime import datetime
from Crawler.config.settings import SYNC_INTERVAL, UPDATE_INTERVAL, CHECKING_TIME
from Crawler.config.utils import write_log
from Crawler.news.news_api import NewsFeed, get_news_dynamic_data
from Crawler.config.insert_relationship import InsertRelationship
from Crawler.news.db_connection import create_noticia, update_noticia
from Crawler.analysis.classifier import Classifier

class ThreadLauncher(object):
    """ Proceso principal, encargado de lanzar y controlar los threads """
    
    def __init__(self):
        write_log('info', 'Iniciando el crawler de noticias')
        self.active_feeds = {}
        self.active_update = None

    def syncronize(self):
        """ Sincronizar los nuevos feeds creados en la base de datos. Es
            decir, lanzar nuevos threads para todos los feeds que todavia
            no tengan un proceso asignado """
        write_log('info', 'Sincronizando paginas de noticias ...')
        print('Thread launcher syncronize...')
        DB_feeds = db.stored_procedure(db.STP_FILTRAR_FEEDS_REQ)
#         print(DB_feeds)
        for row in DB_feeds:
#             print(row)
            print('leyendo ',row.ds_fuente,'...')
            is_active = int(row.yn_activo)
            unique_id = row.id_Pagina_Noticia
#             print(datetime.strptime(row.fch_ini,'%Y-%m-%d')  )
#             print(date.today())
#             print(row.fch_ini < date.today())
            if is_active:# and row.fch_ini <= date.today() <= row.fch_fin:
                if not unique_id in self.active_feeds:
                    print('no esta activo\nagregando',row.ds_fuente,'...')
                    self.active_feeds[unique_id] = FeedThread(row)
                    self.active_feeds[unique_id].start()
                    print(row.ds_fuente,'agregado')
                else:
                    print('actualizando ',row.ds_fuente,'...')
                    self.active_feeds[unique_id].refresh_request_data(row)
                    print(row.ds_fuente,' actualizado')
            else:
                if unique_id in self.active_feeds.keys():
                        self.kill(self.active_feeds, unique_id)
        print("Medios Online totales corriendo (hilos): ",len(self.active_feeds))

        write_log('info','Medios Online totales corriendo (hilos): %s' %(str(len(self.active_feeds))))
        write_log('info', 'Terminada la sincronizacion con la base de datos.')

    def update(self):
        """ Obtener las noticias a actualizar de la base de datos. Luego,
            lanzar un thread que actualize los datos de estas noticias """
        write_log('info', 'Actualizando noticias ...')
        print('Thread launcher update...')
        rows = db.stored_procedure(db.STP_FILTRAR_NOTICIAS)
        if rows:
            if self.active_update and self.active_update.is_alive():
                write_log('warning', 'Peticion de actualizacion cuando todavia se esta ejecutando la anterior actualizacion. Ignorando peticion.')
            else:
                self.active_update = UpdateNewsThread(rows)
                self.active_update.start()
        else:
            write_log('info', 'No hay ninguna noticia para actualizar.')

    def kill(self, threads, thread_id):
        """ Matar un thread y esperar hasta que acabe. """
        threads[thread_id].is_active = False
        threads[thread_id].join()
        del threads[thread_id]

    def killall(self):
        """ Matar todos los threads """
        for thread in self.active_feeds.values():
            thread.is_active = False
        for thread in self.active_feeds.values():
            thread.join()
        if self.active_update:
            self.active_update.is_active = False
            self.active_update.join()
        sys.exit(1)

    def run(self):
        """ Encargado de invocar a la sincronizacion y actualizacion cada
            intervalo de tiempo (especificado en SYNC_INTERVAL y
            UPDATE_INTERVAL) """
        print('Thread Launcher run...')
        start_sync_time = time() - SYNC_INTERVAL
        start_update_time = time() - UPDATE_INTERVAL
        while True:
            if time() - start_sync_time >= SYNC_INTERVAL:
                self.syncronize()
                start_sync_time = time()
            if time() - start_update_time >= UPDATE_INTERVAL:
                self.update()
                start_update_time = time()
            sleep(CHECKING_TIME)

class LoopingThread(threading.Thread):
    """ Subclase de Thread, encargada de invocar a main_func (debe ser definida
        en las clases heredadas) cada intervalo de tiempo, mientras este dentro
        del rango de fechas limite definido. """

    def __init__(self):#, db_row):
        super(LoopingThread, self).__init__()
        self.obj_insert = None
        self.hour_diff = 0
        #self.refresh_request_data(db_row)
        self.is_active = True
        self.classifier = Classifier()
    
    def refresh_request_data(self, db_row):
        self.start_date = db_row.fch_ini
        self.end_date = db_row.fch_fin
        self.interval = db_row.hora_req * 3600 + db_row.min_req * 60
        self.hour_diff = db_row.diferencia_horas
        self.obj_insert.update()

    def main_func(self):
        raise NotImplementedError("'main_func' function has to be defined.")

    def run(self):
        """ Invoca a main_func cada intervalo de tiempo, mientras este dentro
            del rango de fechas limite definido. """
        start_time = time() - self.interval
        while self.is_active: #and self.start_date <= date.today() <= self.end_date:
            if time() - start_time >= self.interval:
                try:
                    self.main_func()
                except Exception as ex:
                    write_log('error', '%s: %s', ex.__class__.__name__, ex)
                    print(ex)
                start_time = time()
            sleep(CHECKING_TIME)

class FeedThread(LoopingThread):
    """ Encargado de crawlear los feeds de noticias """

    def __init__(self, db_row):
        super(FeedThread, self).__init__()#db_row)
        self.id_pagina_notica = db_row.id_Pagina_Noticia
        self.id_fuente = db_row.id_Fuente
        self.ds_feed = db_row.dir_web
        self.is_normal = int(db_row.yn_comportamiento)
        self.feed = NewsFeed(db_row.dir_web,
                             category=db_row.categoria_html,
                             summary=db_row.resumen_html,
                             text=db_row.contenido_html,
                             tags=db_row.tags_html)
        self.last_news_id = None
        self.obj_insert = InsertRelationship(db_row.id_Fuente, db_row.ds_fuente)
        self.refresh_request_data(db_row)
        
        write_log('info', 'Lanzando thread para "%s", cada %s hora(s) y %s minuto(s)', self.ds_feed, db_row.hora_req, db_row.min_req)
        print(db_row.ds_fuente,': inicializacion terminada')
        
    def main_func(self):
        """ Jalar todas las noticias que nos da el RSS del link pasado """
        #print(self.last_news_id)
        all_news = self.feed.get_news(self.last_news_id if self.is_normal else None)
        total_nuevas = 0
        for news in all_news:
            total_nuevas+=1
            #print('\n','noticia nº ',total_nuevas)
            create_noticia(news, self.classifier, self.obj_insert, self.hour_diff, self.is_normal, self.id_fuente)
            
        write_log('info', 'Obtenida(s) %s nuevas noticia(s) de "%s"', total_nuevas, self.ds_feed)
        db.stored_procedure(db.STP_ALMACENAR_CTRL_FEED,
                            self.id_pagina_notica,
                            datetime.now().strftime("%Y-%d-%m %H:%M:%S").split('.')[0],
                            total_nuevas)
        if all_news:
            self.last_news_id = all_news[0].hex_id
    
    def run(self):
        super(FeedThread, self).run()
        write_log('info', 'Terminado el thread para "%s"', self.ds_feed)

class UpdateNewsThread(threading.Thread):
    """ Encargado de actualizar los datos "dinamicos" de todas las noticias
        pasadas """

    def __init__(self, db_rows):
        super(UpdateNewsThread, self).__init__()
        self.all_news = db_rows
        self.is_active = True
        self.condition = threading.Condition()
        self.classifier = Classifier()
        write_log('info', 'Lanzando thread para actualizar los datos dinamicos de %s noticia(s)', len(self.all_news))

    def run(self):
        """ Esta es la funcion en si encargada de iterar por todas las noticias
            pasadas y actualizar sus datos """
        start = time()
        
        consumers = []
        for i in range(4):
            consumers.append(UpdateNewsConsumer(self.all_news,self.condition))
            consumers[i].start()
        print(consumers)

            
        for consumer in consumers:
            consumer.join()
            
        if self.is_active:
            total = time() - start
            write_log('info', 'Demoro %.3f segundos en actualizar %s noticia(s) (%.3f segundos por noticia)', total, len(self.all_news), total/len(self.all_news))


class UpdateNewsConsumer(threading.Thread):
    """thread que trae datos dinamicos de una noticia"""
    def __init__(self,rows,condition):
        super(UpdateNewsConsumer, self).__init__()
        self.all_news = rows
        self.condition = condition
        self.classifier = Classifier()
        
    def run(self):
        while len(self.all_news):
            self.condition.acquire()
            print(len(self.all_news))
            news = self.all_news.pop()
            self.condition.release()
            news_data = get_news_dynamic_data(news.url, news.hex_id,
                                    comments=news.comentarios_html,
                                    facebook_count=news.face_html,
                                    twitter_count=news.Tweets_html,
                                    gplus_count=news.gplus_html)
            update_noticia(self.classifier, news, news_data)
        
    
if __name__ == '__main__':
    launcher = ThreadLauncher()
    launcher.run()
