#-*- coding:utf-8 -*-

import warnings
from Crawler.config.utils import configure_logging, write_log
from Crawler.news.launcher import ThreadLauncher

if __name__ == '__main__':
    configure_logging()
    launcher = ThreadLauncher()
    with warnings.catch_warnings():
        # Ignorar los warnings de SQLAlchemy
        warnings.simplefilter("ignore")
        try:
            launcher.run()
        except KeyboardInterrupt:
            print('sssss')
            write_log('info', 'CTRL-C! Terminando todos los threads ...')
            launcher.killall()

