from twitter import Api, TwitterError

from utils import write_log

MAX_COUNT = 200
MAX_PER_PAGE = 100

REPLY = 'reply'
RETWEET = 'retweet'
ORIGINAL = 'original'

def get_api(settings):
    """ Obtener un objeto Api de twitter autenticado con la cuenta
        pasado por parametro """
    return Api(consumer_key=settings.consumer_key,
               consumer_secret=settings.consumer_secret,
               access_token_key=settings.access_token,
               access_token_secret=settings.access_token_secret,
               input_encoding='utf8')

def get_tweet_type(tweet, api=None):
    """ Obtener el tipo (y padre, si corresponde) de un tweet.
        Puede ser "reply", "retweet" o "original" (no consume requests) """
    api = api or Api()
    if tweet.text.startswith('RT @'):
        return RETWEET, None
    if tweet.in_reply_to_status_id:
        return REPLY, tweet.in_reply_to_status_id
    return ORIGINAL, None

RATE_LIMIT_EXCEEDED_MSG = \
    'Rate limit exceeded. Clients may not make more than 350 requests per hour.'

def get_tweet_by_id(tweet_id, api=None):
    """ Obtener un tweet especificando su id de Twitter (consume un request) """
    api = api or Api()
    try:
        return api.GetStatus(tweet_id)
    except TwitterError as error:
        if str(error) == RATE_LIMIT_EXCEEDED_MSG:
            raise
        write_log('warning', u'No se encontro el comentario con id_Twitter=%s (Error: "%s")', tweet_id, str(error))

def get_full_user(user, api=None):
    """ Obtener la informacion completa de una pagina (usuario), si es que esta
        incompleta (consume un request) """
    NEEDED_KEYS = ['id', 'screen_name', 'friends_count', 'followers_count',
                   'location', 'lang']
    user_dict = user.AsDict()
    if all(key in user_dict for key in NEEDED_KEYS):
        return user
    api = api or Api()
    return api.GetUser(user.screen_name)

def get_replies(screen_name, since_dict, api=None):
    """ Obtener los replies de la pagina (usuario) pasada por parametro
        (consume un request) """
    api = api or Api()
    user = u'@%s' % screen_name
    all_replies = []
    for lang, since_id in since_dict.items():
        replies = api.GetSearch(user, per_page=MAX_PER_PAGE, lang=lang,
                                since_id=since_id)
        if replies:
            since_dict[lang] = replies[0].id
            all_replies += replies
    return [reply for reply in all_replies if reply.text.startswith(user)
                                              and reply.in_reply_to_status_id]

