import sys
import threading
from time import time, sleep
from datetime import date, datetime

from settings import SEARCH_LANGS, GEOCODES, SYNC_INTERVAL, CHECKING_TIME, CUENTA_TWITTER
from utils import normalize_string, write_log
from twitter_api import get_api, get_replies, MAX_COUNT, MAX_PER_PAGE
from db_connection import create_pagina, create_comentario_tw
import database as db
from classifier import Classifier
from insert_keyword import InsertKeyword
GEOCODES = GEOCODES or [None]

class ThreadLauncher(object):
    """ Proceso principal, encargado de lanzar y controlar los threads """
    
    def __init__(self):
        write_log('info', 'Iniciando el crawler')
        self.active_keywords = {}
        self.active_paginas = {}

    def syncronize(self):
        """ Sincronizar los nuevos keywords y paginas creadas en la base
            de datos. Es decir, lanzar nuevos threads para todos los keywords
            y paginas que todavia no tengan un proceso asignado """
        
        write_log('info', 'Sincronizando keywords ...')
        for row in db.stored_procedure(db.STP_FILTRAR_KW_REQ, CUENTA_TWITTER):
            is_active = int(row.yn_Activo)
            unique_id = row.id_RelKeyword
            if is_active and row.fch_ini <= date.today() <= row.fch_fin:
                if not self.active_keywords.has_key(unique_id):
                    self.active_keywords[unique_id] = KeywordThread(row)
                    self.active_keywords[unique_id].start()
                else:
                    self.active_keywords[unique_id].refresh_request_data(row)
            else:
                if self.active_keywords.has_key(unique_id):
                    self.kill(self.active_keywords, unique_id)

        write_log('info', 'Sincronizando paginas ...')
        for row in db.stored_procedure(db.STP_FILTRAR_PAG_REQ, CUENTA_TWITTER):
            is_active = int(row.yn_Activo)
            unique_id = row.id_Cliente_Pagina
            if is_active and row.fch_ini <= date.today() <= row.fch_fin:
                if not self.active_paginas.has_key(unique_id):
                    self.active_paginas[unique_id] = PaginaThread(row)
                    self.active_paginas[unique_id].start()
                else:
                    self.active_paginas[unique_id].refresh_request_data(row)
            else:
                if self.active_paginas.has_key(unique_id):
                    self.kill(self.active_paginas, unique_id)
        
        write_log('info', 'Terminada la sincronizacion con la base de datos.')

    def kill(self, threads, thread_id):
        """ Matar un thread y esperar hasta que acabe. """
        threads[thread_id].is_active = False
        threads[thread_id].join()
        del threads[thread_id]

    def killall(self):
        """ Matar todos los threads """
        for thread in self.active_keywords.values():
            thread.is_active = False
        for thread in self.active_paginas.values():
            thread.is_active = False
        for thread in self.active_keywords.values():
            thread.join()
        for thread in self.active_paginas.values():
            thread.join()
        sys.exit(1)

    def run(self):
        """ Encargado de invocar a la sincronizacion cada intervalo de 
            tiempo (especificado en SYNC_INTERVAL) """
        start_time = time() - SYNC_INTERVAL
        while True:
            if time() - start_time >= SYNC_INTERVAL:
                start_time = time()
                self.syncronize()
            sleep(CHECKING_TIME)

class LoopingThread(threading.Thread):
    """ Subclase de Thread, encargada de invocar a main_func (debe ser definida
        en las clases heredadas) cada intervalo de tiempo, mientras este dentro
        del rango de fechas limite definido. """

    def __init__(self, db_row):
        super(LoopingThread, self).__init__()
        self.refresh_request_data(db_row)
        self.is_active = True
        self.classifier = Classifier()
        self.obj_insert = None
        
    def refresh_request_data(self, db_row):
        self.start_date = db_row.fch_ini
        self.end_date = db_row.fch_fin
        self.interval = db_row.hora_req * 3600 + db_row.min_req * 60
        self.craw_padre = int(db_row.craw_padre)
        self.craw_usuario = int(db_row.craw_usuario)
        self.api = get_api(db_row)

    def main_func(self):
        raise NotImplementedError("'main_func' function has to be defined.")

    def run(self):
        """ Invoca a main_func cada intervalo de tiempo, mientras este dentro
            del rango de fechas limite definido. """
        start_time = time() - self.interval
        while self.is_active and self.start_date <= date.today() <= self.end_date:
            if time()- start_time >= self.interval:
                start_time = time()
                #try:
                self.main_func()
                #except Exception as ex:
            #      write_log('error', '%s: %s', ex.__class__.__name__, ex)
            sleep(CHECKING_TIME)

class KeywordThread(LoopingThread):
    """ Encargado de crawlear por termino (keyword) """

    def __init__(self, db_row):
        super(KeywordThread, self).__init__(db_row)
        self.id_keyword = db_row.id_RelKeyword
        self.ds_keyword = db_row.ds_Keyword
        self.parsed_keyword = normalize_string(self.ds_keyword)
        self.search_dict = dict.fromkeys([(geo, lang) for lang in SEARCH_LANGS
                                                      for geo in GEOCODES])
        self.obj_insert = InsertKeyword(db.stored_procedure(db.STP_SEL_CLIENTE_BY_KEYWORD, self.id_keyword)[0].id_Cliente, int(db_row.tp_opcion))
         
        write_log('info', 'Lanzando thread para el keyword "%s", cada %s hora(s) y %s minuto(s)', self.ds_keyword, db_row.hora_req, db_row.min_req)

    def main_func(self):
        """ Buscar tweets que contengan el termino definido y almacenarlos
            en la base de datos. Por ultimo, guardar el id del ultimo tweet
            obtenido """
        all_tweets = []
        for geocode, lang in self.search_dict:
            tweets = self.api.GetSearch(self.parsed_keyword, lang=lang,
                                        per_page=MAX_PER_PAGE, geocode=geocode,
                                        since_id=self.search_dict[geocode, lang])
            for tweet in tweets:
                create_comentario_tw(self.classifier, self.api, tweet, keyword=self.id_keyword,
                                     save_parent=self.craw_padre,
                                     save_full_user=self.craw_usuario,
                                     obj_insert=self.obj_insert)
            if tweets:
                self.search_dict[geocode, lang] = tweets[0].id
                all_tweets += tweets
        write_log('info', 'Obtenidos %s tweets para el keyword "%s"', len(all_tweets), self.ds_keyword)
        db.stored_procedure(db.STP_ALMACENAR_CTRL_KW,
                         self.id_keyword,
                         str(datetime.now()).split('.')[0],
                         len(all_tweets))
    
    def run(self):
        super(KeywordThread, self).run()
        write_log('info', 'Terminado el thread para el keyword "%s"', self.ds_keyword)
    
class PaginaThread(LoopingThread):
    """ Encargado de crawlear por pagina (usuario) """

    def __init__(self, db_row):
        super(PaginaThread, self).__init__(db_row)
        self.id_cliente_pagina = db_row.id_Cliente_Pagina
        self.id_pagina = db_row.id_Pagina
        self.ds_pagina = db_row.ds_Pagina
        self.last_tweet_id = None
        self.last_reply_id = dict.fromkeys(SEARCH_LANGS)
        self.obj_insert = InsertKeyword(db.stored_procedure(db.STP_SEL_CLIENTE_BY_PAGE, self.id_cliente_pagina)[0].id_Cliente)
        write_log('info', 'Lanzando thread para la pagina "%s", cada %s hora(s) y %s minuto(s)', self.ds_pagina, db_row.hora_req, db_row.min_req)

    def main_func(self):
        """ Buscar tweets de la pagina (usuario) definido y almacenarlos
            en la base de datos. Luego, obtener todos los replies de dicha
            pagina y nuevamente, almacenarlos en la base de datos. Por ultimo,
            guardar el id del ultimo tweet obtenido """
        tweets = self.api.GetUserTimeline(self.ds_pagina, count=MAX_COUNT,
                                          since_id=self.last_tweet_id)
        for tweet in tweets:
            create_comentario_tw(self.classifier, self.api, tweet, pagina_craw=self.id_pagina,
                                 save_parent=self.craw_padre,
                                 save_full_user=self.craw_usuario,
                                 obj_insert=self.obj_insert
                                 )
        if tweets:
            self.last_tweet_id = tweets[0].id
        write_log('info', 'Obtenidos %s tweets para la pagina "%s"', len(tweets), self.ds_pagina)
        replies = get_replies(self.ds_pagina, self.last_reply_id, self.api)
        for reply in replies:
            create_comentario_tw(self.classifier, self.api, reply, pagina_craw=self.id_pagina,
                                 save_parent=self.craw_padre,
                                 save_full_user=self.craw_usuario,
                                 obj_insert=self.obj_insert
                                 )
        write_log('info', 'Obtenidos %s replies para la pagina "%s"', len(replies), self.ds_pagina)
        db.stored_procedure(db.STP_ALMACENAR_CTRL_PAG,
                         self.id_cliente_pagina,
                         str(datetime.now()).split('.')[0],
                         len(tweets),
                         len(replies))

    def run(self):
        super(PaginaThread, self).run()
        write_log('info', 'Terminado el thread para la pagina "%s"', self.ds_pagina)

