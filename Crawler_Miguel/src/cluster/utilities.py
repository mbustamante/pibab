'''
Created on 24/04/2012

@author: hp
'''
import random

def random_number_list(size, a, b):
    number_list = list()
    for i in range(size):
        number = random.randrange(a, b)
        while number in number_list:
            number = random.randrange(a, b)
        number_list.append(number)
    return number_list

def euclidian_distance(vector1, vector2):
        sum_square = 0.0
        for i in range(vector1.get_size()):
            sum_square += (vector1.get_elements()[i] - vector2.get_elements()[i])**2
        distance = sum_square**0.5
        return distance

def cosine_distance(vector1, vector2):
        numerador = 0
        x = 0
        y = 0
        for i in range(vector1.get_size()):
            numerador += vector1.get_elements()[i] * vector2.get_elements()[i]
            x += vector1.get_elements()[i]**2
            y += vector2.get_elements()[i]**2
        denominador = (x * y)**0.5
        return ( 1 - (numerador / denominador))

def create_matrix_distance_euclidian(vector_list):
        n = len(vector_list)  
        matrix=[]
        for k in range(n):
            matrix.append([0]*n)          
        for i in range(n-1):
            j = i +1
            while j  < n:
                distance = euclidian_distance(vector_list[i],vector_list[j])
                matrix[i][j] = distance
                matrix[j][i] = distance
                j += 1                  
        #print(matrix)
        return matrix
    
def create_matrix_distance_cosine(vector_list):
        n = len(vector_list)  
        matrix=[]
        for k in range(n):
            matrix.append([0]*n)          
        for i in range(n-1):
            j = i +1
            while j  < n:
                distance = cosine_distance(vector_list[i],vector_list[j])
                matrix[i][j] = distance
                matrix[j][i] = distance
                j += 1
        #print(matrix)                
        return matrix
    
if __name__ == '__main__':
    lista = random_number_list(4, 0, 3)
    for a in lista:
        print(a)