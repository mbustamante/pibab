'''
Created on 25/04/2012

@author: hp
'''
from vector import Vector
import math
import copy


class Dunn(object):
    
    def __init__(self, matrix_distance):
        self.__matrix_distance = matrix_distance
    
    def get_index(self, cluster_list):
        num = self.get_numerador(cluster_list)
        den = self.get_denominador(cluster_list)
        index = num / den
        return index
    
    def get_numerador(self, cluster_list):
        min_dis = 1000
        size = len(cluster_list)
        for i in range(size):
            id1 = cluster_list[i].get_id_centroid()
            j = i + 1
            while j < size:
                id2 = cluster_list[j].get_id_centroid()
                dis = self.__matrix_distance[id1][id2]
                if dis < min_dis:
                    min_dis = dis
                j += 1
        return min_dis  
      
    def get_denominador(self, cluster_list):
        max_dis = 0
        for cluster in cluster_list:
            id_centroid = cluster.get_id_centroid()
            for vector in cluster.get_vector_list():
                id_vector = vector.get_id()
                dis = self.__matrix_distance[id_centroid][id_vector]
                if dis > max_dis:
                    max_dis = dis
        return max_dis
   

class DunnAverage(object):
    
    def __init__(self, matrix_distance):
        self.__matrix_distance = matrix_distance
    
    def get_index(self, cluster_list):
        k = len(cluster_list)
        m = k*(k-1) / 2
        num = self.get_numerador(cluster_list)
        num /= m
        den = self.get_denominador(cluster_list)
        den /= k
        index = num / den
        return index
        
    def get_numerador(self, cluster_list):
        total_dis = 0
        size = len(cluster_list)
        for i in range(size):
            id1 = cluster_list[i].get_id_centroid()
            j = i + 1
            while j < size:
                id2 = cluster_list[j].get_id_centroid()
                total_dis += self.__matrix_distance[id1][id2]
                j += 1
        return total_dis
     
    def get_denominador(self, cluster_list):
        total_dis = 0
        for cluster in cluster_list:
            id_centroid = cluster.get_id_centroid()
            max_dis = 0
            for vector in cluster.get_vector_list():
                id_vector = vector.get_id()
                dis = self.__matrix_distance[id_centroid][id_vector]
                if dis > max_dis:
                    max_dis = dis
            total_dis += max_dis
        return total_dis
 
class Dem(object):
    
    def __init__(self, matrix_distance, type_distance):
        if type_distance == 'cosine': self.__matrix_similarity = self.cosine_similarity_matrix(matrix_distance)
        elif type_distance == 'euclidian': self.__matrix_similarity = self.euclidian_similarity_matrix(matrix_distance)
        self.__dem_index = 0
        
    def get_index(self, cluster_list, vector_list):
        self.evaluate(cluster_list, vector_list)
        return self.__dem_index
    
    def evaluate(self, cluster_list, vector_list):
        expected_density = 0
        V = len(self.__matrix_similarity)
        weigth = self.get_weigth_similarity(vector_list)
        density = self.get_density(weigth, V)
        
        for cluster in cluster_list:
            vector_list = copy.deepcopy(cluster.get_vector_list())
            vector_list.append(cluster.get_centroid())
            Vi = len(vector_list)
            WGi = self.get_weigth_similarity(vector_list)
            expected_density += (Vi/V) * (WGi/(Vi**density))
        self.__dem_index = expected_density
            
                
    def euclidian_similarity_matrix(self, matrix_distance):
        matrix = copy.deepcopy(matrix_distance)
        n = len(matrix_distance)
        max_value = 0  
        for i in range(n-1):
            j = i + 1
            while j  < n:
                if matrix_distance[i][j] > max_value:
                    max_value = matrix_distance[i][j]
                j += 1          
        for i in range(n-1):
            j = i + 1
            while j  < n:
                matrix[i][j] = 1 - (matrix_distance[i][j] / max_value)
                matrix[j][i] = 1 - (matrix_distance[j][i] / max_value)
                j += 1               
        return matrix
    
    def cosine_similarity_matrix(self, matrix_distance):
        matrix = copy.deepcopy(matrix_distance)
        n = len(matrix_distance)        
        for i in range(n-1):
            j = i + 1
            while j < n:
                matrix[i][j] = 1 - matrix_distance[i][j]
                matrix[j][i] = 1 - matrix_distance[j][i]
                j += 1               
        return matrix

    def get_weigth_similarity(self, vector_list):
        sum_total = 0
        n = len(vector_list)
        for i in range(n-1):
            j = i + 1
            id1 = vector_list[i].get_id()
            while j < n:
                id2 = vector_list[j].get_id()
                sum_total += self.__matrix_similarity[id1][id2]
                j += 1
        sum_total += n
        return sum_total
    
    def get_density(self, weigth, v, base=10):
        return math.log(weigth, base) / math.log(v, base)
        
if __name__ == '__main__':
    vector_list = list()
    v1 = Vector(0,"v0",[2,1,0,0])
    v2 = Vector(1,"v1",[0,0,2,1])
    v3 = Vector(1,"v1",[0,0,2,2])
    vector_list.append(v1)
    vector_list.append(v2)
    vector_list.append(v3)
    obj = Dem(vector_list,[[0,2,3],[2,0,4],[3,4,0]])
    #obj.evaluate(vector_list)
    