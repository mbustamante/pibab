'''
Created on 24/04/2012

@author: hp
'''
from k_means_medoid import K_Means
from utilities import random_number_list


class BestSeeds(object):
    '''
    classdocs
    '''

    def __init__(self, k):
        '''
        Constructor
        '''
        self.__k = k
        self.__iterations = 10
        self.__best_seeds = list()
        self.__initial_seeds_list = list()
        self.__max_index = 0
    
    def evaluate(self, vector_list, type_distance, type_index):
        for i in range(self.__iterations):
            initial_seeds = random_number_list(self.__k, 0, len(vector_list))
            while self.__exist_seed_list(initial_seeds):
                initial_seeds = random_number_list(self.__k, 0, len(vector_list))
            #print(initial_seeds)
            self.__initial_seeds_list.append(initial_seeds)
            obj = K_Means(self.__k, initial_seeds)
            obj.evaluate(vector_list, type_distance)
            index = obj.get_index(type_index, type_distance)
            obj.print_cluster()
            print(index, "-.-.-.-.-.-.-.-.-.-.-")
            if index > self.__max_index:
                self.__best_seeds = initial_seeds
                self.__max_index = index
        print("k :",self.__k,"indice :",self.__max_index)
        
    def get_max_index(self):
        return self.__max_index
    
    def get_best_seeds(self):
        return self.__best_seeds
       
    def __exist_seed_list(self, seed_list):
        for initial_seeds in self.__initial_seeds_list:
            for seed in initial_seeds:
                if  not seed in seed_list:
                    return False
        if len(self.__initial_seeds_list) == 0: return False
        return True

class BestK(object):
    '''
    classdocs
    '''

    def __init__(self, k_initial, k_end):
        '''
        Constructor
        '''
        self.__k_initial = k_initial
        self.__k_end = k_end
        self.__max_index = 0
        self.__best_k = k_initial
        self.__best_seeds = None
        self.__vector_list = None
    
    def evaluate(self, vector_list, type_distance, type_index):
        self.__vector_list = vector_list
        iterations = self.__k_end - self.__k_initial + 1
        for i in range(iterations):
            k = self.__k_initial + i
            obj = BestSeeds(k)
            obj.evaluate(vector_list, type_distance, type_index)
            index = obj.get_max_index()
            if index > self.__max_index:
                self.__max_index = index
                self.__best_k = k
                self.__best_seeds = obj.get_best_seeds()     
    
    def get_best_result(self):
        print("<<<<<<<<<<final>>>>>>>>>")
        print("k:", self.__best_k)
        print("indice:", self.__max_index)
        obj = K_Means(self.__best_k, self.__best_seeds)
        obj.evaluate(self.__vector_list, 'euclidian')
        obj.print_cluster()            
        