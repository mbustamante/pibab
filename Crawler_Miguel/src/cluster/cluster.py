'''
Created on 17/04/2012

@author: Roque Lopez
'''
from vector import Vector

class Cluster(object):
    
    def __init__(self, name):
        self.__id_centroid = -1
        self.__name = name
        self.__vector_list = list()
        self.__centroid = Vector(0,'0',[])
        
    def  add_vector(self, vector):
        self.__vector_list.append(vector)
        
    def  remove_vector(self, id_v):
        for i in range(len(self.__vector_list)):
            if self.__vector_list[i].get_id() == id_v:
                del self.__vector_list[i]
                return

    def  get_vector(self, id_v):
        for vector in self.__vector_list:
            if vector.get_id() == id_v:
                return vector
        
    def set_centroid(self, centroid):
        self.__centroid = centroid
    
    def set_id_centroid(self, id_c):
        self.__id_centroid = id_c
    
    def get_centroid(self):
        return self.__centroid
    
    def get_id_centroid(self):
        return self.__id_centroid
    
    def clear_vector_list(self):
        self.__vector_list = []
        
    def get_size(self):
        return len(self.__vector_list)
    
    def get_vector_list(self):
        return self.__vector_list
    
    def get_name(self):
        return self.__name
    
        
    
        