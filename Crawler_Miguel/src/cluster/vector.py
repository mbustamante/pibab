'''
Created on 17/04/2012

@author: Roque Lopez
'''

class Vector(object):

    def __init__(self, id_v, name, elements):
        self.__id = id_v
        self.__name = name
        self.__elemnts = elements
        
    def get_size(self):
        return len(self.__elemnts)
    
    def get_elements(self):
        return self.__elemnts
    
    def get_id(self):
        return self.__id
    
    def get_name(self):
        return self.__name
        