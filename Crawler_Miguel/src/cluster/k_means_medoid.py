'''
Created on 17/04/2012

@author: Roque Lopez
'''
from cluster import Cluster
from indexes import Dunn, DunnAverage, Dem
from utilities import create_matrix_distance_cosine, create_matrix_distance_euclidian
import copy

class K_Means(object):
    '''
    classdocs
    '''

    def __init__(self, k, initial_seeds):
        self.__k = k
        self.__vector_list = list()
        self.__cluster_list = list()
        self.__matrix_distance = list()
        self.__initial_seeds = initial_seeds
    
    def evaluate(self, vector_list, type_distance):
        self.__vector_list = vector_list
        self.__create_cluster()
        self.__create_initial_centroid()
        if type_distance == 'cosine': self.__matrix_distance = create_matrix_distance_cosine(self.__vector_list)
        elif type_distance == 'euclidian': self.__matrix_distance = create_matrix_distance_euclidian(self.__vector_list)
        else: return
        #print(">>iteracion 0")
        #self.print_cluster()
        i = 1
        while True:
            self.__update_cluster()
            old_cluster_list = copy.deepcopy(self.__cluster_list)
            self.__update_centroid()
            #print(">>iteracion "+str(i))
            #self.print_cluster()
            i += 1
            if self.__verify_final(old_cluster_list, self.__cluster_list):
                return
                
    def __create_cluster(self):
        for i in range(self.__k):
            cluster = Cluster("cluster "+str(i))
            self.__cluster_list.append(cluster)
            
    def __create_initial_centroid(self):
        for i in range(self.__k):
            index = self.__initial_seeds[i]
            self.__cluster_list[i].set_centroid(self.__vector_list[index])
            self.__cluster_list[i].set_id_centroid(self.__vector_list[index].get_id())

    def __update_cluster(self):
        self.__clear_cluster()
        
        for vector in self.__vector_list:
            min_dis = 100000#self.__matrix_distance[i][self.__cluster_list[0].get_id_centroid()] + self.__matrix_distance[i][self.__cluster_list[1].get_id_centroid()]       
            id_vector = vector.get_id()
            if not self.__is_centroid(id_vector): # si el vector que analizamos no es centroide
                index_cluster = 0
                i = 0
                for cluster in  self.__cluster_list:
                    id_centroid = cluster.get_id_centroid()
                    distance = self.__matrix_distance[id_centroid][id_vector]
                    if distance < min_dis:
                        index_cluster = i
                        min_dis = distance
                    i += 1
                self.__cluster_list[index_cluster].add_vector(vector)

    def __is_centroid(self, id_c):
        for cluster in self.__cluster_list:
            if cluster.get_id_centroid() == id_c:
                return True
        return False
              
    def __clear_cluster(self):
        for i in range(len(self.__cluster_list)):
            self.__cluster_list[i].clear_vector_list()
                              
    def __update_centroid(self):
        for i in range(self.__k):
            self.__choose_new_centroid(i)
                
    def __choose_new_centroid(self, k):
        id_centroid = self.__cluster_list[k].get_id_centroid()  
        min_dis = self.__distance_in_cluster(id_centroid, self.__cluster_list[k])
        #print("min dis = ",min_dis)
        id_new_centroid = id_centroid

        for vector1 in self.__cluster_list[k].get_vector_list():
            id_v1 = vector1.get_id()
            total_dis = self.__distance_in_cluster(id_v1, self.__cluster_list[k])
            total_dis += self.__matrix_distance[id_v1][id_centroid] # sumamos la distancia hacia el centroid
            #print("new dis = ", total_dis)
            if total_dis < min_dis: # hay un nuevo posible centroide
                id_new_centroid = id_v1
                min_dis = total_dis

        if id_new_centroid != id_centroid:
            new_centroid = self.__cluster_list[k].get_vector(id_new_centroid)
            self.__cluster_list[k].remove_vector(id_new_centroid)
            self.__cluster_list[k].add_vector(self.__cluster_list[k].get_centroid())
            self.__cluster_list[k].set_centroid(new_centroid)
            self.__cluster_list[k].set_id_centroid(id_new_centroid)
            
            
    def __distance_in_cluster(self, id_to_me, cluster):
        dis = 0
        for vector in  cluster.get_vector_list():
            id_vector = vector.get_id()
            dis += self.__matrix_distance[id_to_me][id_vector]
        return dis                       
            
    def __verify_final(self, old_cluster_list, current_cluster_list):
        for i in range(len(old_cluster_list)):
            if old_cluster_list[i].get_id_centroid() !=  current_cluster_list[i].get_id_centroid():
                return False
        return True
    
    def get_index(self, type_measure, type_distance):
        if type_measure == 'dem':
            obj = Dem(self.__matrix_distance, type_distance)
            return obj.get_index(self.__cluster_list, self.__vector_list)
        elif type_measure == 'dunn':
            obj = Dunn(self.__matrix_distance)
            return obj.get_index(self.__cluster_list)
        else:
            print('no existe ese tipo de indice')
            return
    
    def print_cluster(self):
        for cluster in self.__cluster_list:
            elements = ""
            for vector in cluster.get_vector_list():
                elements += vector.get_name()+" "
            size = len(cluster.get_vector_list()) + 1
            print(" CENTRO: "+cluster.get_centroid().get_name()+"  ELEM: "+elements, size)
            
    def print_intial_seeds(self):
        for i in range(self.__k):
            index = self.__initial_seeds[i]
            print(self.__vector_list[index].get_name())
               
    