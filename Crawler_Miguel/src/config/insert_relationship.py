#-*- coding:utf-8 -*-
'''
Created on 24/05/2012
@author: Roque Lopez
@contact: rlopezc27@gmail.com
'''
import re
import config.data_base as db
from config.utils import normalize_string
from config.settings import MARCA, COMPETENCIA, SINONIMO, ALERT_TODO, ALERT_KEYWORD
from demon_process.send_mail import Sender, HTML_Creator


class InsertRelationship(object):

    def __init__(self, id_cliente):
        self.id_cliente = id_cliente
        self.tupla_relkeyword_grupo_lema = None
                            
    def search(self, comment, keyword):
        comment = normalize_string(comment.lower())
        keyword = keyword.lower()
        keyword_list = keyword.split(" ")
        for key in keyword_list:
            if comment.count(key) < 1:
                return False              
        return True
    
    def send_message(self, id_alert, comment, url, source, followers):
        users = db.stored_procedure(db.STP_SEL_USUARIOS_ALERTA, id_alert)
        if len(users) == 0:
            return
        obj = HTML_Creator()
        body = obj.create_alert(comment, url)
        subject = "Alarma - Lindexa producido por %s (%s seguidores) en Facebook" % (source, followers) if followers > 0 else "Alarma - Lindexa producido por %s en Facebook" % (source)
        user_list = ["soporte@lindexa.com"]
        for user in users:
            user_list.append(user.email)
        obj_sender = Sender("lindexa.soporte@gmail.com", "ladrillo27", "smtp.gmail.com")
        try:
            print("Enviando mensajes")
            obj_sender.send_mail(subject, body, user_list)
            obj_sender.close()
        except Exception as e:
            print("ERROR No se pudo enviar el mensaje. '%s'" %(e))   
           
    def search_gap(self, comment, keyword, windows):
        keyword_list = {key:[] for key in re.split("\s", keyword)}
        word_list = re.split("\s", comment)
        n = len(keyword_list)
        m = len(word_list)
        range_size = (windows*(n-1)) + n
        pos = 0
        
        for word in word_list:
            if word in keyword_list:
                keyword_list[word].append(pos)
            pos += 1
        key = None
        
        if not self.has_elements(keyword_list):
            return False
            
        while True:
            print(keyword_list)
            my_list = []
            min_position = m
            for tmp in keyword_list.items():
                my_list.append(tmp[1][0])
                if  tmp[1][0] < min_position:
                    min_position = tmp[1][0]
                    key = tmp[0]
            print(key, min_position)
            print(min_position, min_position+range_size-1)
            print(my_list)
            keyword_list[key].pop(0)
            if self.is_in_range(my_list, min_position, min_position+range_size-1):
                return True   
            if not self.has_elements(keyword_list):
                return False
            
    def is_in_range(self, my_list, min_value, max_value):
        for element in my_list:
            if element < min_value or element > max_value:
                return False
        return True
    
    def has_elements(self, my_list):
        for element in my_list.values():
            if len(element) == 0:
                return False
        return True
    

class InsertRelationshipKeyword(InsertRelationship): 
      
    def __init__(self, id_cliente, id_rel_keyword, opc_relkeyword):
        super().__init__(id_cliente)
        self.__id_rel_keyword = id_rel_keyword
        self.__opc_relkeyword = opc_relkeyword
        self.update()
        
    def insert_all_by_keyword(self, id_comment, comment):
        db.stored_procedure(db.STP_INS_TODO_X_KEYWORD, id_comment, self.__id_rel_keyword, self.__opc_relkeyword, comment, self.tupla_relkeyword_grupo_lema)
 
    def verify_by_keyword(self, id_comment, comment, url):
        row = db.stored_procedure(db.STP_SEL_ALERTA_X_SEGUIDORES, self.__id_rel_keyword)[0]
        id_alert, followers_condition  = row.id_alerta, row.num_seguidores
        try:
            num_followers = db.stored_procedure(db.STP_SEL_SEGUIDORES, id_comment)[0].num_seguidores# Solo para Peru
        except:
            return
        if num_followers >= followers_condition:
            print("Alerta por seguidores, entre id_alerta %s y id_comentario %s" %(id_alert, id_comment))
            db.stored_procedure(db.STP_INS_REL_ALERTA_COMENTARIO, id_alert, id_comment)
            row = db.stored_procedure(db.STP_SEL_PAGINA_X_COMENTARIO, id_comment)[0] 
            self.send_message(id_alert, comment, url, row.ds_nombre, row.num_seguidores)
    
    def update(self):
        if self.__opc_relkeyword == SINONIMO:
            group_type =  db.stored_procedure(db.STP_SEL_MARCA_COMPETENCIA, self.__id_rel_keyword)[0].tp_grupo
            self.tupla_relkeyword_grupo_lema = self.__get_tupla_by_keyword(group_type)
                   
    def __get_tupla_by_keyword(self, group_type):
        text_list = []
        for row in db.stored_procedure(db.STP_SEL_TUPLA_X_KEYWORD, self.id_cliente, group_type):
            if row.id_RelKeyword != None and row.ds_Lema != None:
                text_list.append(str(row.id_RelKeyword)+','+row.ds_Lema)
        if len(text_list) == 0:
            return None
        print(text_list)
        return ";".join(text_list)
            
                        
class InsertRelationshipPage(InsertRelationship):      
    
    def __init__(self, id_cliente, id_group):
        super().__init__(id_cliente)
        self.__influential_list =  None
        self.update(id_group)        
        
    def insert_all_by_page(self, id_comment, comment):
        if self.tupla_relkeyword_grupo_lema is not None:
            db.stored_procedure(db.STP_INS_TODO_X_PAGINA, id_comment, comment, self.tupla_relkeyword_grupo_lema)
            
    def verify_by_page(self, id_comment, comment, url, page):
        """ Verifica si se ha producido una alerta en una cuenta crawleada, ya sea por TODO o por KEYWORD"""
        try:
            row = db.stored_procedure(db.STP_SEL_ALERTA_X_CUENTA, self.id_cliente, page)[0]
        except:
            return
        id_alert, status  = row.id_alerta, row.estado
        if status == ALERT_TODO:
            print("Alerta por cuenta-todo, entre id_alerta %s y id_comentario %s" %(id_alert, id_comment))
            db.stored_procedure(db.STP_INS_REL_ALERTA_COMENTARIO, id_alert, id_comment)
            row = db.stored_procedure(db.STP_SEL_PAGINA_X_COMENTARIO, id_comment)[0] 
            self.send_message(id_alert, comment, url, row.ds_nombre, row.num_seguidores)
        
        if status == ALERT_KEYWORD:
            keyword_list = db.stored_procedure(db.STP_SEL_KEYWORDS_DE_ALERTA, id_alert)
            for keyword in keyword_list:
                if self.search(comment, keyword.ds_lema):
                    print("Alerta por cuenta-keyword, entre id_alerta %s y id_comentario %s" %(id_alert, id_comment))
                    db.stored_procedure(db.STP_INS_REL_ALERTA_COMENTARIO, id_alert, id_comment)
                    row = db.stored_procedure(db.STP_SEL_PAGINA_X_COMENTARIO, id_comment)[0] 
                    self.send_message(id_alert, comment, url, row.ds_nombre, row.num_seguidores) 
    
    def verify_by_influential(self, id_comment, ds_comment, url, id_page):
        for influential in self.__influential_list:
            if id_page == influential.id_pagina:
                db.stored_procedure(db.STP_INS_REL_ALERTA_COMENTARIO, influential.id_alerta, id_comment)
                self.send_message(influential.id_alerta, ds_comment, url, influential.ds_nombre, influential.num_seguidores) 
                
        
    def update(self, id_group):
        """ Actualiza la lista de influyentes y tuplas de keywords"""
        self.__influential_list =  db.stored_procedure(db.STP_SEL_INFLUYENTES, self.id_cliente)
        text_list = self.__get_tupla_by_page(id_group)
        print(text_list)
        if len(text_list) == 0:
            self.tupla_relkeyword_grupo_lema = None
        else:
            self.tupla_relkeyword_grupo_lema = ";".join(text_list)
            
    def __get_tupla_by_page(self, id_group):
        text_list = []                                   
        for synonymous in db.stored_procedure(db.STP_SEL_TUPLA_X_SINONIMO, self.id_cliente):
            if synonymous.id_RelKeyword != None and synonymous.ds_Lema != None:
                text_list.append(str(synonymous.id_RelKeyword)+','+synonymous.ds_Lema)
                if id_group != synonymous.id_grupo:
                    for row in db.stored_procedure(db.STP_SEL_TUPLA_X_PAGINA, synonymous.id_grupo):
                        if row.id_RelKeyword != None and row.ds_Lema != None:
                            text_list.append(str(row.id_RelKeyword)+','+row.ds_Lema+' '+synonymous.ds_Lema)
        
        for row in db.stored_procedure(db.STP_SEL_TUPLA_X_PAGINA, id_group):
            if row.id_RelKeyword != None and row.ds_Lema != None:
                text_list.append(str(row.id_RelKeyword)+','+row.ds_Lema)
                
        return text_list
                
                                              
if __name__ == '__main__':

    obj = InsertRelationship(None)
    comment = "c z z z z a z z z b b z z a"
    keyword = "b a c"
    print(obj.search_gap(comment, keyword, 2))
    a = ['1', '2']
    b = None
    a += b
    print(a)
#    print(obj.search("probando con varios keywords para el crawler", "yo keyword"))
#    print("a".count("b"))