# -*- coding: utf-8 -*-
'''
Created on Feb 9, 2012
@author: Roque Lopez
@contact: rlopezc27@gmail.com
'''
''' <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Package Analysis >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> '''

# DICTIONARY_* son archivos que contienen las palabras mas frecuentes de un idioma
DICTIONARY_SPANISH = "../resource/dictionary_spanish.txt"
DICTIONARY_ENGLISH = "../resource/dictionary_english.txt"

# LEMATIZER_* son archivos que contienen los lemas
LEMATIZER_SPANISH = "../resource/lematizer_spanish.txt"
LEMATIZER_ENGLISH = "../resource/lematizer_english.txt"

# STOPWORDS_* son archivos que contienen los stopwords
STOPWORDS_SPANISH = "../resource/stopwords_spanish.txt"
STOPWORDS_SPANISH_OPINION_MINING = "../resource/stopwords_spanish_opinion_mining.txt"
STOPWORDS_ENGLISH = "../resource/stopwords_english.txt"

# Recursos Lexicos para el analisis de sentimiento
EMOTICONS = "../resource/dictionary/emoticons.txt"
BOOSTER_WORDS_SPANISH = "../resource/dictionary/booster_words_spanish.txt"
SENTIMENT_WORDS_SPANISH = "../resource/dictionary/sentiment_words_spanish.txt"
SLANGS_PERUVIAN = "../resource/dictionary/slangs_peruvian.txt"
NEGATING_WORDS_SPANISH = "../resource/dictionary/negating_words_spanish.txt"
COMBINATIONS_SPANISH  = "../resource/dictionary/combinations_spanish.txt"
COMBINATIONS_SLANGS_PERUVIAN = "../resource/dictionary/combinations_slangs_peruvian.txt"
PUNCTUATION = "../resource/dictionary/punctuation.txt"

# Tipos de Terminos
TERM_TYPE_EMOTICON = 'emoticon'
TERM_TYPE_BOOSTER = 'booster'
TERM_TYPE_WORD_SLANG = 'word_slang'
TERM_TYPE_COMBINATION = 'combination'
TERM_TYPE_NEGATING = 'negating'
TERM_TYPE_PUNCTUATION = 'punctuation'
TERM_TYPE_NEUTRO = 'neutro'

# Simbolos adicionales
FLEXIS_SIMBOL = '#'
SPLITTER_WEIGHTS = '=='
ENCODING = 'utf-8'
TERM_NOT_FOUND = ''
SPLITTER_FREQUENT_WORD = '<##>'

''' <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Package Crawler >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> '''

# Datos de la coneccion a la base de datos.
#ENGINE =  '{SQL Server Native Client 10.0}'
ENGINE = 'FreeTDS'

# Nombre de la base de datos.
DATABASE = 'BD_Lindexa_Amazon_Final'#'roque'

# Host donde esta alojada la base de datos ('localhost' si es una base de datos local).
#HOST = 'ITSERVER\SQL2008R2'#'PPSERVER\SQLSERVER2008'
HOST = 'itsoftware.no-ip.org' 

#Puerto para conexión
PORT= '1460'

# username para conectarse con la base de datos.
USER = 'sa'
# password del username, usado en la conexion con la base de datos.
PASSWORD = 'ladrillo00.'
#
#ENGINE =  '{SQL Server Native Client 10.0}'
#
## Nombre de la base de datos.
#DATABASE = 'BD_Lindexa'#'roque'
#
## Host donde esta alojada la base de datos ('localhost' si es una base de datos local).
#HOST = 'wrugy4lqbw.database.windows.net'#'PPSERVER\SQLSERVER2008'
#
## username para conectarse con la base de datos.
#USER = 'lind_adm@wrugy4lqbw'
## password del username, usado en la conexion con la base de datos.
#PASSWORD = 'pjGrupo$8142'


# Tipos de posts
TYPE_PUBLICATION = 1
TYPE_COMMENT = 2

#Tipos de origen de la tabla Nube
TYPE_GROUP = 1
TYPE_KEYWORD = 2
TYPE_MANUAL_TAG = 3

# Tipos de Nube
NUBE_UNIGRAMA = 1
NUBE_BIGRAMA = 2
NUBE_TAG_MANUAL = 3
TYPE_MFS = 4

CUENTA_TWITTER = 1
CUENTA_FACEBOOK = 2
COMENTARIO_TWITTER = 1
COMENTARIO_FACEBOOK = 2
KEYWORD = 1
PAGE = 2

MARCA = 1
COMPETENCIA = 2
SINONIMO = 3

ALERT_TODO = 1
ALERT_KEYWORD = 2

EACH_24_HOURS = 1
EACH_12_HOURS = 2

DAILY = 1
WEEKLY = 2
MONTHLY = 3
