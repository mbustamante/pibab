# -*- coding: utf-8 -*-
import re
from datetime import timedelta, datetime
from unicodedata import normalize
from urllib.parse import urlparse
from isodate import parse_datetime
from analysis.corpus import stopwords_list
from config.settings import STOPWORDS_SPANISH
TIME_DIFF = -18000 # GTM-05:00 Bogota, Lima, Quito

spanish_stopwords_list = stopwords_list(STOPWORDS_SPANISH)

def urlcreator(scheme, domain, path='', params=[]):
    """ Crea una URL 
        scheme : tipo de protocolo Ejm: http, https, etc.
        domain : Dominio base de la url.
        path : directorio a acceder en tal dominio.
        params : parametros extras a preguntar en una URL.
    """
    extra1 = r'://'
    extra2 = r'/'   
    rs = scheme + extra1 + domain + extra2
    if path != '':
        rs += path
    if len(params) > 0:
        rs += r'?'
        rs += '&'.join(params)
    return rs

def getUrlParameters(myurl):
    """ Obtiene los parametros en una URL con parametros. Retorna un diccionario con los parametros como claves"""
       
    purl = urlparse(myurl)
    splitted = (purl.query).split('&')
    rs = {}
    for i in splitted:
        sp = i.split('=')
        if len(sp) > 1:
            rs[sp[0]] = sp[1]
    return rs

def get_facebook_datetime(string):
    """ Retorna un objeto datetime sin su TimeZone """
    facebook_datetime =  parse_datetime(string.split('+')[0])
    real_datetime = facebook_datetime + timedelta(seconds=TIME_DIFF)
    return real_datetime

def timetounixtime(obj_time):
    """ Retorna un objeto datetime en formato unix Time(en segundos) """
    since = datetime(1970,1,1,0,0,0) #1970 01 01 00:00:00 UTC
    delta = (obj_time - since)
    return delta.days*86400 + delta.seconds

def normalize_string(string):
    """ Reemplazar caracteres especiales unicode por caracteres ascii """
    return str(normalize('NFKD', string).encode('ascii', 'ignore'), 'utf-8')


def normalize_string_sp(string):
    """ Elimina caracteres especiales no admitidos al llamar a un procedimiento almacenado"""
    string = string.replace('{', ' ')
    string = string.replace('}', ' ')
    string = string.replace('"', ' ')
    string = string.replace('\\', ' ')
    string = string.replace('$', ' ')
    return normalize_string(string)

def write_log(level, message, *args):
    """ Escribir un mensaje en el log, especificando su nivel """
    file = open("../resource/log.txt", 'a', encoding='utf-8')
    file.write( level+" "+message+" ".join(args)+"\n")
    file.close()

def remove_punctuation_marks(word):
    """ Elimina los signos de puntuacion de un texto """
    if re.match("^[a-z0-9\xE1\xE9\xED\xF3\xFA\xF1]+$", word):
        return word
    else:
        new_word = ""
        for i in range(len(word)):
            if re.match("[\w\xE1\xE9\xED\xF3\xFA\xF1]", word[i]):
                new_word += word[i]
        return new_word
def remove_whitespaces(text):
    cleaned_text = re.sub('\s+', ' ', text)
    return cleaned_text.strip()
 
def get_clear_text(text):
    """ Elimina los signos de puntuacion y stopswords de un texto """
    text = text.lower()
    text_list = []
    words = re.split("\s+",text) # Divide el texto por cualquier caracter en blanco
    for word in words:
        word = remove_punctuation_marks(word)
        if len(word) > 1 and (not word in spanish_stopwords_list) :
            text_list.append(word)
    return " ".join(text_list)