# -*- coding: utf-8 -*-
'''
Created on 11/04/2012

@author: Roque Lopez
'''
import win32com.client
from time import sleep


def get_location(url):
    ''' Retorna la direccion de un usuario '''
    tmp_text = get_all_text(url)
    text = str(tmp_text, 'utf-8')
    pattern_list = [r'Vive en <a href="http://www.facebook.com/pages/', r'De <a href="http://www.facebook.com/pages/']

    for pattern in pattern_list:
        index = text.find(pattern)
        if index > 0:
            index = index + len(pattern)
            index2 = text[index:].find("/")
            location = text[index:index + index2]
            return get_city_and_country(location)
    return'sin-direccion'

def get_all_text(url):
    """
    Given a url, it starts IE, loads the page, gets the HTML.
    Works only in Win32 with Python Win32com extensions enabled.
    Needs IE. Why? If you�re forced to work with Brain-dead 
    closed sourceapplications that go to tremendous length to deliver
    output specific to browsers; and the application has no interface
    other than a browser; and you want get data into a CSV or XML
    for further analysis;
    Note: IE internally formats all HTML to stupid mixed-case, no-
    quotes-around-attributes syntax. So if you are planning to parse
    the data, make sure you study the output of this function rather
    than looking at View-source alone.
    """
 
    #if you are calling this function in a loop, it is more
    #efficient to open ie once at the beginning, outside this
    #function and then use the same instance to go to urls
    ie = win32com.client.Dispatch("InternetExplorer.Application")
 
    ie.Visible = 1 #make this 0, if you want to hide IE window
    #IE started
    ie.Navigate(url)
    #it takes a little while for page to load. sometimes takes 5 sec.
    if ie.Busy:
        sleep(5)
    #now, we got the page loaded and DOM is filled up
    #so get the text
    text = ie.Document.body.innerHTML
    #text is in unicode, so get it into a string
    text = text.encode('utf-8','ignore')
    #save some memory by quitting IE! **very important** 
    ie.Quit()
    #print(text)
    return text

def get_city_and_country(location):
    ''' Retorna la ciudad y pais; recibe la direccion completa '''
    array = location.split("-")
    size = len(array)
    if size > 1:
        return array[size-2]+"-"+array[size-1]
    return location
           
if __name__ == '__main__':
    id_list = ["sharhorodska", "100001346455303","nicolasmendozadelsolar","100001185957914","590234147","rlopezc27"]
    for id1 in id_list:
        url = "http://www.facebook.com/"+id1
        print(get_location(url))
