# -*- coding: utf-8 -*-
'''
Created on Feb 2, 2012

@author: Roque Lopez
'''
#from analysis.lemmatizer import Stemmer
#from analysis.spelling_corrector import Corrector
import config.settings as settings
import analysis.corpus as corpus
import re

class CleanText (object):
    
    def __init__(self):
        self.__stopwords_list = corpus.stopwords_list(settings.STOPWORDS_SPANISH)
        #self.__stemmer = Stemmer(settings.LEMATIZER_SPANISH)
        #self.__corrector = Corrector(settings.DICTIONARY_SPANISH)
        
    def get_clear_text(self, text):
        text = text.lower()
        text_list = []
        words = re.split("\s",text) # Divide el texto por cualquier caracter en blanco
        for word in words:
            #tmp =  word
            word = self.__remove_punctuation_marks(word)
            #if (len(word) == 0): word = tmp
            word = self.__remove_repeated_letters(word)
            if not word in self.__stopwords_list:
                #word = self.__corrector.correct(word)
                #word = self.__stemmer.get_stem(word)
                text_list.append(word)
        return " ".join(text_list)
        
    def __remove_repeated_letters(self, word):
        size = len(word)
        i = 0
        new_word = ""
        while i < size:
            new_word += word[i]
            is_repeated = False
            while(i < size-1 and word[i] == word[i+1]):
                i += 1
                is_repeated = True
            if is_repeated:
                new_word += word[i]
            i += 1
        return new_word
    
    def __remove_punctuation_marks(self, word):
        if re.match("^[a-z0-9\xE1\xE9\xED\xF3\xFA\xF1]+$", word):
            return word
        else:
            size = len(word)
            my_iter = 0
            new_word = ""
            while(my_iter< size):
                if re.match("[a-z\xE1\xE9\xED\xF3\xFA\xF1]", word[my_iter]):
                    new_word += word[my_iter]
                my_iter += 1
            return new_word
    
if __name__ == '__main__':
    
    text = r"Porque me siguen // factu//rando si hace 2 meses pedi el corte de servicio áño núévíó también qué tú"
    obj = CleanText()
    print (obj.get_clear_text(text))