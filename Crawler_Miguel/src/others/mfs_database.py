'''
Created on 04/07/2012

@author: Dev6
'''
from analysis.clean_text import CleanText
from analysis.frequent_word import MFS
from config.data_base import *

STP_SEL_COMENTARIO = 'stp_sel_ComentarioByCliente1'
STP_INS_MFS = 'stp_ins_MFS1'
STP_INS_REL_MFS_COMENTARIO = 'stp_ins_RelMFSComentario'

def analize_mfs(id_cliente, fch_inicio, fch_fin, umbral, top, mes, anio, id_grupo):
    rows = stored_procedure(STP_SEL_COMENTARIO,id_cliente, fch_inicio, fch_fin, id_grupo)
    clean_text_list = []
    clean_text = None
    tupla_list = [] # tupla id_comentario, comentario_clean
    obj_cleaner = CleanText()
    print ("Cleaning Text...") 
    for row in rows:
        clean_text = obj_cleaner.get_clear_text(row.ds_Comentario)
        clean_text_list.append(clean_text)
        tupla_list.append([row.id_Comentario, clean_text])
    text = " <##> ".join(clean_text_list) 
    print("Nro de comentarios = ", len(rows))
    print ("MFS Evaluating...")
    obj_mfs = MFS(text, umbral)
    mfs_list = obj_mfs.get_top(top)
    insert(mfs_list, tupla_list, id_cliente, mes, anio,id_grupo)
    
def insert(mfs_list, tupla_list, id_cliente, mes, anio, id_grupo):
    for mfs in mfs_list:
        #print(mfs[0], mfs[1])
        id_mfs = stored_procedure(STP_INS_MFS, mfs[0], mfs[1], 370)[0].id_mfs
        for tupla in tupla_list:
            #print(tupla[0], tupla[1])
            if tupla[1].count(mfs[0]) > 0:
                stored_procedure(STP_INS_REL_MFS_COMENTARIO, id_mfs, tupla[0], id_cliente, mes, anio)
    
if __name__ == '__main__':
    id_cliente = 18
    fch_inicio = '2010-07-01 00:00:00'
    fch_fin =  '2013-07-30 23:59:59'
    umbral_min_frec = 10
    top_mfs = 100
    anio = None
    mes = None
    id_grupo = 331
    analize_mfs(id_cliente, fch_inicio, fch_fin, umbral_min_frec, top_mfs, mes, anio, id_grupo)

    