# -*- coding: utf-8 -*-
'''
Created on Feb 2, 2012

@author: r-27
'''
from tmp_data import Comment
from clean_text import CleanText
from frequent_word import MFS
from frequent_word import NGrama
from code import frequent_word

class PreProcessing (object):
        
    def __init__(self):
        self.__cleaner = CleanText()
        self.__comments_list = dict()
        self.__id = 0
        self.__CLEAN_TEXT=""
      
    def read_data(self, file_path):
        text = self.__read_file(file_path)
        array_publications = text.split("<pp>")
        
        for publication in array_publications:
            if len(publication.strip()) > 1:
                cont = 0
                id_root = -1
                array_comments = publication.split("<ss>")
                size = len(array_comments)
                while(cont < size ):
                    if (cont==0):
                        clean_comment = self.__cleaner.get_clear_text(array_comments[cont].split("<ff>")[0])
                        obj = Comment(self.__id, id_root, array_comments[cont],clean_comment)
                        id_root = self.__id
                    else:
                        clean_comment = self.__cleaner.get_clear_text(array_comments[cont].split("<ff>")[0])
                        obj = Comment(self.__id, id_root, array_comments[cont],clean_comment)
                
                    self.__comments_list[self.__id]= obj
                    self.__CLEAN_TEXT += clean_comment+" "+frequent_word.SPLITTER+" "
                    self.__id += 1
                    cont += 1
        print ("Number of comments", self.__id)
        
    def __read_file(self, file_path):
        my_file = open(file_path,"r",encoding='utf-8')
        text = my_file.read()
        my_file.close()
        return text                                     
        
    def __save_file(self, type1, top_list, file_name):
        out_file = open(file_name, "a")
        for obj in top_list:
            mfs = obj[0]
            frequency = obj[1]
            for comment in self.__comments_list.values():
                clean_text = comment.get_cleantext()
                if clean_text.count(mfs) > 0:
                    out_file.write(type1+"<ss>"+mfs+"<ss>"+str(frequency)+"<ss>"+str(comment.get_id())+"<ss>"+str(comment.get_id_root())+"<ss>"+comment.get_text()+"<pp>")
        out_file.close()
    
    def analize_mfs(self, top, threshold, file_name):
        mfs = MFS(self.__CLEAN_TEXT.strip(),threshold)
        top_list= mfs.get_top(top)
        self.__save_file("4", top_list, file_name)
    
    def analize_ngrama(self, n_length, top, threshold, file_name):
        ngrama = NGrama(self.__CLEAN_TEXT.strip(),threshold,n_length)
        top_list= ngrama.get_top(top)
        self.__save_file(str(n_length), top_list, file_name)
    