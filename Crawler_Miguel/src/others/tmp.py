# -*- coding: utf-8 -*-
'''
Created on Feb 21, 2012

@author: Dev6
'''
import time
import re
def __remove_punctuation_marks(word):
        if re.match("^[\w\xE1\xE9\xED\xF3\xFA\xF1]+$", word):
            return word
        else:
            new_word = ""
            for i in range(len(word)):
                if re.match("[\w\xE1\xE9\xED\xF3\xFA\xF1]", word[i]):
                    new_word += word[i]
            return new_word
        
if __name__ == '__main__':
    print ("Starting... ",time.strftime("%H:%M:%S %d %b"))
    print ("Cleaning Text...")
    print (__remove_punctuation_marks("http://www.google.com.pe/"))
    print (__remove_punctuation_marks("...abc"))
    print (__remove_punctuation_marks("ábc..."))
    print (__remove_punctuation_marks("...abc..."))
    print (__remove_punctuation_marks("...ABC121..."))
    print (__remove_punctuation_marks("...a.b.c..."))
    print ("Finished... ",time.strftime("%H:%M:%S %d %b"))
    a = "!!!!!!!!"
    i= a.find("!")
    print(i)
    print(a[:i])
    print(a[i:])
    x = {'a':1, 'b': 2}
    y = {'b':10, 'c': 11}
    z4 = {}
    z4.update(x)
    z4.update(y)
    for a in z4.items():
        print(a)
    w = ["z","x","y","a"]
    for t in range(10):
        print(t)
    term = "hola?"
    index = term.find("!") or term.find("?")
    print(index)
    size = 4
    for i in range(size):
        j = i + 1
        for j in range(size):
            print(i, j)
    for i in range(size):
        j = i + 1
        while j  < size:
            print(i, j)
            j += 1
