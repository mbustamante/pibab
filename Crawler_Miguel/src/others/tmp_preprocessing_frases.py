# -*- coding: utf-8 -*-
'''
Created on Feb 2, 2012

@author: r-27
'''
from clean_text import CleanText
from frequent_word import MFS
from frequent_word import NGrama
from code import frequent_word

class PreProcessing2 (object):
        
    def __init__(self):
        self.__cleaner = CleanText()
        self.__comments_list = dict()
        self.__id = 0
        self.__CLEAN_TEXT=""
      
    def read_data(self, file_path, phrase):
        text = self.__read_file(file_path)
        array_publications = text.split("<pp>")
        
        for publication in array_publications:
            if len(publication.strip()) > 1:
                array_comments = publication.split("<ss>")
                tmp_phrase = array_comments[1]
                if tmp_phrase == phrase:
                    comment = array_comments[5].split("<ff>")[0]
                    clean_comment = self.__cleaner.get_clear_text(comment)
                    self.__id += 1
                    self.__CLEAN_TEXT += clean_comment+" "+frequent_word.SPLITTER+" "
                
        print ("Number of comments", self.__id)
        
    def __read_file(self, file_path):
        my_file = open(file_path,"r");
        text = my_file.read()
        my_file.close()
        return text                                     
        
    def __save_file(self, type1, top_list, file_name):
        out_file = open(file_name, "a")
        for obj in top_list:
            mfs = obj[0]
            frequency = obj[1]
            out_file.writelines(type1+" , "+mfs+" , "+str(frequency)+"\n")
        out_file.close()
    
    def analize_mfs(self, top, threshold, file_name):
        mfs = MFS(self.__CLEAN_TEXT.strip(),threshold)
        top_list= mfs.get_top(top)
        self.__save_file("4", top_list, file_name)
    
    def analize_ngrama(self, n_length, top, threshold, file_name):
        ngrama = NGrama(self.__CLEAN_TEXT.strip(),threshold,n_length)
        top_list= ngrama.get_top(top)
        self.__save_file(str(n_length), top_list, file_name)

if __name__ == '__main__':
    import time
    print ("Starting... ",time.strftime("%H:%M:%S %d %b"))
    list_phrases = {"movistar tv"}
    file_name ="canalmovistar_mfs"
    for phrase in list_phrases:
        print ("Cleaning Text...")
        obj_PreProcessing = PreProcessing2()
        obj_PreProcessing.read_data("../resource/"+file_name+".txt", phrase)
        print ("Evaluating...")
        out_file="../resource/"+file_name+phrase+".txt"
        obj_PreProcessing.analize_mfs(100, 10, out_file)
        obj_PreProcessing.analize_ngrama(3, 100, 10, out_file)
        obj_PreProcessing.analize_ngrama(2, 100, 10, out_file)
        obj_PreProcessing.analize_ngrama(1, 100, 70, out_file)
    print ("Finished... ",time.strftime("%H:%M:%S %d %b"))
    