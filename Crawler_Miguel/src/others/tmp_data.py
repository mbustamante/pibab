# -*- coding: utf-8 -*-
'''
Created on Feb 6, 2012

@author: r-27
'''
class Comment (object):
    
    def __init__(self, id1, id_root, text, cleantext):
        self.__id = id1
        self.__id_root = id_root
        self.__text = text
        self.__cleantext = cleantext
        
    def get_id(self):
        return self.__id
    
    def get_id_root(self):
        return self.__id_root
    
    def get_text(self):
        return self.__text
    
    def get_cleantext(self):
        return self.__cleantext