Obama en Día de la Independencia de EE.UU.: “Somos una nación de inmigrantes”

El presidente dijo esto ante militares que se nacionalizaron estadounidenses en un acto en la Casa Blanca
Washington (EFE). El presidente estadounidense, Barack Obama, afirmó hoy que “la historia de los inmigrantes es nuestra propia historia” en una ceremonia por el Día de la Independencia en la que 25 integrantes de las Fuerzas Armadas, varios de ellos latinoamericanos, se naturalizaron.

“Somos una nación de inmigrantes. Los inmigrantes nos han hecho más fuertes y más prósperos”, aseguró Obama durante el acto celebrado en la sala Este de la Casa Blanca.

ESTADOS UNIDOS SERÁ SIEMPRE JOVEN
Obama destacó la fecha, el 4 de julio, en la que se conmemora la independencia del país, como especialmente apropiada para realizar este acto en el que los veinticinco miembros de las fuerzas armadas pasaron a tener la ciudadanía estadounidense.

“Ninguna otra nación se renueva y refresca tan constantemente como Estados Unidos, gracias a la llegada de inmigrantes. Por eso Estados Unidos siempre es joven”, dijo Obama ante una audiencia compuesta por las familias de los naturalizados.

Los nuevos ciudadanos proceden de 17 países, desde China o Filipinas a Rusia, Nigeria o Camerún; además de una decena de latinoamericanos provenientes de Bolivia, Honduras, Guatemala, México, El Salvador y Colombia.

*DÍA DE EMOCIONES *
Silvano Carcamo, natural de Honduras, se mostraba visiblemente emocionado acompañado de su hijo y su esposa.

“Es increíble, quería dejarle una buena historia a mi hijo. He peleado por el país, quería que se valorase más la ciudadanía”, explicó Carcamo, médico de la Armada y recién llegado de Afganistán, tras la ceremonia.

Obama estuvo acompañado en la ceremonia por la secretaria de Seguridad Nacional, Janet Napolitano, y el director de los Servicios de Ciudadanía e Inmigración, Alejandro Mayorkas.

Napolitano agradeció los esfuerzos realizados por los nuevos ciudadanos estadounidenses y subrayó: “las libertades que disfrutamos son consecuencia de los sacrificios realizados por personas como ustedes”.