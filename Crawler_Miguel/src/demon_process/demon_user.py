# -*- coding: utf-8 -*-
import http.cookiejar as cookielib
import urllib.request 
import re
import time
class UserData(object):
    
    def __init__(self):
        self.opener = None
        self.__get_opener()
        
    def __get_opener(self):
        cj = cookielib.CookieJar()
        self.opener = urllib.request.build_opener(urllib.request.HTTPCookieProcessor(cj))       
        login_data = urllib.parse.urlencode({'email' : 'james.po.72@gmail.com', 'pass' : 'acari27'})
        binary_data = login_data.encode('utf-8')
        self.opener.open('http://www.facebook.com')
        self.opener.open('https://login.facebook.com/login.php?login_attempt=1', binary_data)
        
    def get_user_data_facebook(self, url):
        locale = None
        friends = 0
        try:
            text = self.opener.open(url).read()
        except:
            text = ""
            print("==== ERROR %s" % url)
        text = str(text)
#        f=open("tmp.txt","w")
#        f.write(text)
#        f.close()
        pattern_list_locale = [r'Vive en <a href="http://www.facebook.com/pages/', r'De <a href="http://www.facebook.com/pages/']
        pattern_list_friends = [r'Amigos</span><span class="count">(.+)</span></div></a></div></div></li><li class="photos', r'>Amigos \((.{1,5})\)</a>']
        #pattern_list_friends = [r'Amigos</span><span class="count">(.+)</span></div></a></div></div></li><li class="photos', r'>Amigos \((.+)\)</a>']
        
        for pattern in pattern_list_locale:
            index = text.find(pattern)
            if index > 0:
                index = index + len(pattern)
                index2 = text[index:].find("/")
                locale = self.__replace(text[index:index + index2])
                break
            
        for pattern in pattern_list_friends:
            result =  re.search(pattern, text)
            if result:
                friends = result.group(1).replace('.', '')
                break
        return [locale, friends]
        
    def __get_city_and_country(self, location):
        ''' Retorna la ciudad y pais; recibe la direccion completa '''
        array = location.split("-")
        size = len(array)
        if size > 1:
            return array[size-2]+"-"+array[size-1]
        return location
    
    def __replace(self, word):
        if word:
            word= word.replace("%C3%A1", "a")
            word= word.replace("%C3%A9", "e")
            word= word.replace("%C3%AD", "i")
            word= word.replace("%C3%B3", "o")
            word= word.replace("%C3%BA", "u")
            word= word.replace("%C3%B1", "ñ")
        return word 
                
if __name__ == '__main__':
    obj = UserData()
    print("Abriendo pagina...")
    print ("Starting... ",time.strftime("%H:%M:%S %d %b"))
    id_list = ["silloxsillo"]#, "silloxsillo", "596541837", "jawitejada","nicolasmendozadelsolar","sharhorodska","paola.benitesvargas"]
    for id1 in id_list:
        url = "http://www.facebook.com/"+id1
        print(obj.get_user_data_facebook(url))
    print ("Finished... ",time.strftime("%H:%M:%S %d %b"))