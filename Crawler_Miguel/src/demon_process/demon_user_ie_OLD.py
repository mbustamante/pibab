# -*- coding: utf-8 -*-
'''
Created on 11/04/2012
@author: Roque Lopez
@contact: rlopezc27@gmail.com
'''
import re
import win32com.client
from time import sleep
import config.data_base as db


class UserDataIE(object):
    
    def __init__(self):
        pass

    def get_user_data_facebook(self, url):
        ''' Retorna la direccion de un usuario '''
        tmp_text = self.get_all_text(url)
        text = str(tmp_text, 'utf-8')
      
        locale = None
        friends = 0
        pattern_list_locale = [r'Vive en <a href="http://www.facebook.com/pages/', r'De <a href="http://www.facebook.com/pages/']
        pattern_list_friends = [r'Amigos</span><span class="count">(.+)</span></div></a></div></div></li><li id="pagelet_timeline_photos_nav_top', r'>Amigos \((.{1,5})\)</a>']
        
        for pattern in pattern_list_locale:
            index = text.find(pattern)
            if index > 0:
                index = index + len(pattern)
                index2 = text[index:].find("/")
                locale = self.replace(text[index:index + index2])
                break
            
        for pattern in pattern_list_friends:
            result =  re.search(pattern, text)
            if result:
                friends = result.group(1).replace('.', '')
                break
        return locale, friends

    def get_city_and_country(self, location):
        ''' Retorna la ciudad y pais; recibe la direccion completa '''
        array = location.split("-")
        size = len(array)
        if size > 1:
            return array[size-2]+"-"+array[size-1]
        return location
    
    def replace(self, word):
        if word:
            word= word.replace("%C3%A1", "a")
            word= word.replace("%C3%A9", "e")
            word= word.replace("%C3%AD", "i")
            word= word.replace("%C3%B3", "o")
            word= word.replace("%C3%BA", "u")
            word= word.replace("%C3%B1", "ñ")
        return word 
    
    def get_all_text(self, url):
        """ Given a url, it starts IE, loads the page, gets the HTML."""

        while True:
            try:
                ie = win32com.client.Dispatch("InternetExplorer.Application")  
                ie.Visible = 0 
                ie.Navigate(url)
                while ie.Busy:
                    sleep(5)
                text = ie.Document.body.innerHTML
                text = text.encode('ascii','ignore')
                ie.Quit()
                break
            except Exception as e:
                print(e)
                sleep(15)
        return text
           
if __name__ == '__main__':
    obj = UserDataIE()
    page_list = db.stored_procedure(db.STP_SEL_PAGINA_SIN_REGION)
    print("Por actualizar %s paginas"% len(page_list))
    i = 0
    for page in page_list:
        url = "http://www.facebook.com/"+page.ds_pagina
        
        location, friends = obj.get_user_data_facebook(url)
        print(location, friends, page.id_pagina, page.ds_pagina)
        if location or int(friends) > 0:
            db.stored_procedure(db.STP_UPD_REGION_SEGUIDORES, page.id_pagina, friends, location)
        i += 1
        if i >= 100:
            print("Durmiendo 5 minutos...")
            sleep(300)
            i = 0
        
#    id_list = ["sharhorodska", "100001346455303","nicolasmendozadelsolar","rlopezc27"]
#    for id1 in id_list:
#        url = "http://www.facebook.com/"+id1
#        location, friends = obj.get_user_data_facebook(url)
#        print(location, friends)
 
