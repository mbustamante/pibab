# -*- coding: utf-8 -*-
'''
Created on 19/04/2012
@author: Roque Lopez
@contact: rlopezc27@gmail.com
'''
import threading
import calendar
import config.data_base as db
from time import sleep
from datetime import datetime, timedelta
from config.settings import EACH_12_HOURS, EACH_24_HOURS, DAILY, WEEKLY, MONTHLY
from demon_process.send_mail import Sender, HTML_Creator
from config.utils import TIME_DIFF

class Manager(threading.Thread):
    ''' Clase encargada  del control y gestion de los hilos '''

    def __init__(self, interval=5):
        threading.Thread.__init__(self)
        self.__config_list = dict()
        self.__update_interval = interval * 60
        self.__init_data()
    
    def run(self):
        ''' Inicia los hilos '''            
        while True:
            #Threads
            for id_thread in self.__config_list.keys():
                if not self.__config_list[id_thread].get_status():
                    print('info', "iniciando configuracion '%s'" % id_thread)
                    self.__config_list[id_thread].active_status()
                    self.__config_list[id_thread].start()
                        
            sleep(self.__update_interval)
            self.__updating_data()              
               
    def kill_thread(self, threads, id_thread):
        ''' Mata un hilo, dado su id '''
        threads[id_thread].join()
        del threads[id_thread]
            
    def __updating_data(self):
        ''' Consulta a la base de datos si existen nuevas configuraciones de resumenes '''
        print('info', "INICIANDO ACTUALIZACION")    
        #Threads 
        for data in db.stored_procedure(db.STP_SEL_CONF_RESUMENES):
            id_thread = data.idConf_Resumenes
            if id_thread in self.__config_list.keys():
                self.__config_list[id_thread].update_data(data)
            else:
                self.__config_list[id_thread] = Config_Thread(id_thread, data)

    def __init_data(self):
        ''' Inicia el crawler por primera vez '''
        print('info', "INICIANDO DEMON DE RESUMENES")
        #Threads 
        for data in db.stored_procedure(db.STP_SEL_CONF_RESUMENES):
            id_thread = data.idConf_Resumenes
            self.__config_list[id_thread] = Config_Thread(id_thread, data)

class Config_Thread(threading.Thread):
    ''' Clase que representa una configuracion de un resumen '''
    
    def __init__(self, id_thread, data):
        threading.Thread.__init__(self)
        self.__id = id_thread
        self.__id_customer = data.Id_Cliente
        self.__start_hour = data.Hora_inicio
        self.__tp_frequency = data.tp_frecuencia
        self.__yn_alert = data.yn_alerta
        self.__yn_weekly = data.yn_semanal
        self.__yn_monthly = data.yn_mensual
        self.__weekly_hour = data.hora_semanal
        self.__monthly_hour = data.hora_mensual
        self.__weekly_day = data.dia_semanal
        self.__monthly_day = data.dia_mensual
        self.__yn_daily = data.yn_diario
        self.__status = False
        self.__event = threading.Event()
        self.__send = False
        self.__users = db.stored_procedure(db.STP_SEL_USUARIOS_RESUMENES, self.__id)
    
    def get_status(self):
        return self.__status
    
    def active_status(self):
        self.__status = True
    
    def update_data(self, data):
        ''' Actualiza los datos  de una configuracion de un resumen '''
        print("Actualizando datos de la configuracion '%s'" % self.__id)
        self.__start_hour = data.Hora_inicio
        self.__tp_frequency = data.tp_frecuencia
        self.__yn_alert = data.yn_alerta
        self.__yn_weekly = data.yn_semanal
        self.__yn_monthly = data.yn_mensual
        self.__weekly_hour = data.hora_semanal
        self.__monthly_hour = data.hora_mensual
        self.__weekly_day = data.dia_semanal 
        self.__monthly_day = data.dia_mensual
        self.__yn_daily = data.yn_diario
        self.__users = db.stored_procedure(db.STP_SEL_USUARIOS_RESUMENES, self.__id)
        self.__send = False
        self.__event.set()
          
    def run(self):
        while True:
            self.computing()
    
    def computing(self):
        print("Calculando proximo envio...")
        self.__send = True
        my_list = {}
        if self.__yn_daily:
            my_list[DAILY] = self.remaining_time_by_day()            
#        if self.__yn_weekly:
#            my_list[WEEKLY] = self.remaining_time_by_week()   
#        if self.__yn_monthly:
#            my_list[MONTHLY] = self.remaining_time_by_month()
        # sorted_list es una tupla (tipo_resumen, segundos_pa_dormir)
        sorted_list = sorted(my_list.items(), key=lambda x:x[1])
        if len(sorted_list) > 0:       
            self.sleeping(sorted_list[0][1])
            if self.__send and sorted_list[0][1] > 60:
                self.send_message(sorted_list[0])
        else:
            sleep(3600) # una hora
    
    def remaining_time_by_day(self):
        now = datetime.now() #+ timedelta(seconds=TIME_DIFF)
        now_seconds = now.hour*3600 + now.minute*60 + now.second
        target_seconds = self.__start_hour.hour*3600 + self.__start_hour.minute*60 + self.__start_hour.second         
         
        if self.__tp_frequency == EACH_12_HOURS:
            target_seconds1 = (self.__start_hour.hour+12)*3600 + self.__start_hour.minute*60 + self.__start_hour.second 
            if target_seconds > now_seconds:
                return target_seconds - now_seconds
            elif target_seconds1 > now_seconds:
                return target_seconds1 - now_seconds
            else:
                tmp = datetime(now.year, now.month, now.day, self.__start_hour.hour, self.__start_hour.minute, self.__start_hour.second)         
                target = tmp + timedelta(days=1)
        elif self.__tp_frequency == EACH_24_HOURS:
            if target_seconds > now_seconds:
                return target_seconds - now_seconds
            else:
                tmp = datetime(now.year, now.month, now.day, self.__start_hour.hour, self.__start_hour.minute, self.__start_hour.second)         
                target = tmp + timedelta(days=1)
        print("DAILY target =", target)
        return self.remaining_seconds(target, now)
    
    def remaining_time_by_week(self):
        now = datetime.now() #+ timedelta(seconds=TIME_DIFF)
        day_in_week = now.weekday() + 1 # x dennis
        now_seconds = day_in_week*86400 + now.hour*3600 + now.minute*60 + now.second
        target_seconds = self.__weekly_day*86400 + self.__weekly_hour.hour*3600 + self.__weekly_hour.minute*60 + self.__weekly_hour.second   
        
        if target_seconds > now_seconds:
                return target_seconds - now_seconds
        else:
            tmp = datetime(now.year, now.month, now.day, self.__weekly_hour.hour, self.__weekly_hour.minute, self.__weekly_hour.second)         
            remaining_day = self.__weekly_day - day_in_week + 7
            target = tmp + timedelta(days=remaining_day)
        print("WEEKLY target =", target)
        return self.remaining_seconds(target, now)
    
    def remaining_time_by_month(self):
        now = datetime.now() #+ timedelta(seconds=TIME_DIFF)
        day = now.day
        now_seconds = day*86400 + now.hour*3600 + now.minute*60 + now.second
        target_seconds = self.__monthly_day*86400 + self.__monthly_hour.hour*3600 + self.__monthly_hour.minute*60 + self.__monthly_hour.second   
        
        if target_seconds > now_seconds:
                return target_seconds - now_seconds
        else:
            tmp = datetime(now.year, now.month, now.day, self.__monthly_hour.hour, self.__monthly_hour.minute, self.__monthly_hour.second)         
            days_in_month =  calendar.monthrange(now.year, now.month)[1]
            remaining_day = days_in_month - day + self.__monthly_day
        target = tmp + timedelta(days=remaining_day)
        print("MONTHLY target =", target)
        return self.remaining_seconds(target, now)
    
    def remaining_seconds(self, target, now):
        delta = (target - now)
        return delta.days*86400 + delta.seconds
        
    def sleeping(self, seconds):
        try:
            print("info, sleeping configuracion '%s' (%s)" % (self.__id, self.sec_to_time(seconds)))
        except Exception as e:
            print(e)
        self.__event.wait(seconds)
        self.__event.clear()
        
    def sec_to_time(self, seconds):
        d = datetime(1,1,1) + timedelta(seconds=seconds)
        return "%d dias con %d:%d:%d" % (d.day-1, d.hour, d.minute, d.second)
            
    def send_message(self, tupla):
        summary_type = tupla[0]
        if summary_type == DAILY:
            period = self.__tp_frequency
            summary_title = "Diario"           
        elif WEEKLY == summary_type:
            period = self.__weekly_day
            summary_title = "Semanal"    
        else: # summary_type == MONTHLY:
            period = self.__monthly_day
            summary_title = "Mensual" 
        now = datetime.now() #+ timedelta(seconds=TIME_DIFF)
        id_summary = db.stored_procedure(db.STP_SEL_GENERAR_RESUMEN, summary_type, self.__id_customer, now, period, self.__yn_alert)[0].id_resumen
        
        if len(self.__users) == 0:
            return
        obj = HTML_Creator()
        body = obj.create_summary(id_summary, summary_title)
        subject = "Resumen %s - Lindexa" % summary_title 
        good = True  
        
        for user in self.__users:
            try:
                print("Enviando el resumen al usuario '%s'" %(user.nombre))
                obj_sender = Sender("lindexa.soporte@gmail.com", "ladrillo27", "smtp.gmail.com")
                obj_sender.send_mail(subject, body, [user.email])
                obj_sender.close()
                db.stored_procedure(db.STP_INS_ENVIO_EMAIL, id_summary, user.id_usuario, now, user.email)
            except Exception as e:
                print("ERROR No se pudo enviar el mensaje. '%s'" %(e))
                good = False
        if good:
            db.stored_procedure(db.STP_UPD_RESUMEN_CAB, id_summary)
        try:
            obj_sender = Sender("lindexa.soporte@gmail.com", "ladrillo27", "smtp.gmail.com")
            obj_sender.send_mail(subject, body, ["soporte@lindexa.com"])
            obj_sender.close()
        except Exception as e:
            print("ERROR No se pudo enviar el mensaje. '%s'" %(e))
                               
if __name__ == '__main__':
    obj = Manager(120)
    obj.start()

                