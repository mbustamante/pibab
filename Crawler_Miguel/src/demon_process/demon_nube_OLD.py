'''
Created on 04/07/2012
@author: Dev6
@contact: rlopezc27@gmail.com
'''
from analysis.frequent_word import MFS, NGrama
from config.settings import SPLITTER_FREQUENT_WORD, TYPE_KEYWORD, TYPE_GROUP, TYPE_MFS, NUBE_UNIGRAMA, NUBE_BIGRAMA
from config.utils import get_clear_text, normalize_string
import config.data_base as db
from datetime import date

class Demon_Nube(object):
    
    def __init__(self, id_customer):
        self.__id_customer = id_customer
        self.__dictionary = dict()
        self.__comments = dict() # tupla id_comentario, comentario_clean
        self.__create_dictionary() # tupla grupo, lista_relkeywords
        
    def __create_dictionary(self):
        rows = db.stored_procedure(db.STP_SEL_GRUPOS_X_CLIENTE, self.__id_customer)
        for row in rows:
            if row.id_Grupo in self.__dictionary:
                self.__dictionary[row.id_Grupo].append(row.id_RelKeyword)
            else:
                self.__dictionary[row.id_Grupo] = [row.id_RelKeyword]
                              
    def launch(self, start_date, end_date, threshold, top, tp_nube):
        #current_date = date.today()
        current_date = date(2012, 8, 1)
        for group, rel_keywords in self.__dictionary.items():
            global_comment_list = {}
            for rel_keyword in rel_keywords:
                comment_list = self.recorrer_keywords(rel_keyword, start_date, end_date)
                mfs_list = self.get_frequent_list(comment_list, threshold, top, tp_nube)
                self.insert_nube(mfs_list, comment_list, rel_keyword, TYPE_KEYWORD, tp_nube, current_date)
                global_comment_list.update(comment_list)
            mfs_list = self.get_frequent_list(global_comment_list, threshold, top, tp_nube)
            self.insert_nube(mfs_list, global_comment_list, group, TYPE_GROUP, tp_nube, current_date)
                
    def recorrer_keywords(self, rel_keyword, start_date, end_date):
        clean_text_list = {}
        clean_text = None
        comments = db.stored_procedure(db.STP_SEL_COMENTARIOS_X_RELKEYWORD, rel_keyword, start_date, end_date)
        for comment in comments:
            if comment.id_Comentario in self.__comments:
                clean_text_list[comment.id_Comentario] = self.__comments[comment.id_Comentario]
            else:
                clean_text = get_clear_text(normalize_string(comment.ds_Comentario))
                clean_text_list[comment.id_Comentario] = clean_text
                self.__comments[comment.id_Comentario] = clean_text
        return clean_text_list

    def get_frequent_list(self, text_list, threshold, top, tp_nube):
        simbol = " " + SPLITTER_FREQUENT_WORD + " "
        text = simbol.join(text_list.values())
        if tp_nube == TYPE_MFS:
            obj = MFS(text, threshold)
        if tp_nube == NUBE_UNIGRAMA:
            obj = NGrama(text, threshold, 1)
        if tp_nube == NUBE_BIGRAMA:
            obj = NGrama(text, threshold, 2)      
        return obj.get_top(top)
        
           
    def insert_nube(self, mfs_list, comment_list, id_origen, tp_origen, tp_nube, current_date):
        for mfs in mfs_list:
            id_nube = db.stored_procedure(db.STP_INS_NUBE, mfs[0], mfs[1], id_origen, tp_origen, tp_nube, current_date)[0].id_nube
            for comment in comment_list.items():
                if comment[1].count(mfs[0]) > 0:
                    print("relacionando nube %s con el comentario %s" % (id_nube, comment[0]))
                    db.stored_procedure(db.STP_INS_REL_NUBE_COMENTARIO, id_nube, comment[0])
                    
    
if __name__ == '__main__':
    id_cliente = 18
    start_date = '2012-08-01 00:00:00'
    end_date =  '2012-08-01 23:59:59'
    threshold = 2
    top = 10
    obj_demon = Demon_Nube(id_cliente)
    #obj_demon.launch(start_date, end_date, threshold, top, TYPE_MFS)
    obj_demon.launch(start_date, end_date, threshold, top, NUBE_UNIGRAMA)
    top = 10
    obj_demon.launch(start_date, end_date, threshold, top, NUBE_BIGRAMA)
    
    