# -*- coding: utf-8 -*-
'''
Created on 11/04/2012
@author: Roque Lopez
@contact: rlopezc27@gmail.com
'''
import re
import os
import win32com.client
import config.data_base as db
import json
from time import sleep
from datetime import datetime, timedelta
from crawler_facebook.facebook_api import Api
from crawler_facebook.error import backoff


class UserDataIE(object):
    
    def __init__(self):
        pass

    def get_user_data_facebook(self, url):
        ''' Retorna la direccion de un usuario '''
        tmp_text = self.get_all_text(url)
        while tmp_text is None:
            tmp_text = self.get_all_text(url)
        text = str(tmp_text, 'utf-8')
      
        id_fb_location = None
        location = None
        friends = 0
        pattern_list_locale = [r'Vive en <a href="http://www.facebook.com/pages/', r'De <a href="http://www.facebook.com/pages/']
        pattern_list_friends = [r'Amigos</span><span class="count">(.+)</span></div></a></div></div></li><li id="pagelet_timeline_photos_nav_top', r'>Amigos \((.{1,5})\)</a>']
        
        for pattern in pattern_list_locale:
            index = text.find(pattern)
            if index > 0:
                index += len(pattern)
                index2 = text[index:].find("/")
                location = self.replace(text[index:index + index2])
                index += index2 + 1          
                index2 = text[index:].find('"')  
                id_fb_location = text[index:index + index2]
                break
            
        for pattern in pattern_list_friends:
            result =  re.search(pattern, text)
            if result:
                friends = result.group(1).replace('.', '')
                break
        return id_fb_location, location, friends
   
    def get_all_text(self, url):
        """ Given a url, it starts IE, loads the page, gets the HTML."""
        print("Abriendo la pestaña '%s'" % url)
        text = None
        try:
            ie = win32com.client.Dispatch("InternetExplorer.Application")  
            ie.Visible = 0 
            ie.Navigate(url)
            while ie.Busy:
                sleep(5)
            text = ie.Document.body.innerHTML
            text = text.encode('ascii','ignore')
            ie.Quit()
        except Exception as e:
            try:
                os.system("taskkill /im WerFault.exe /f")
            except Exception as e:
                print(e)
            print("Eliminando el proceso 'Internet Explorer' debido al sgte error:", e)
            os.system("taskkill /im iexplore.exe /f")
            
        return text
    
    def replace(self, word):
        if word:
            word= word.replace("%C3%A1", "a")
            word= word.replace("%C3%A9", "e")
            word= word.replace("%C3%AD", "i")
            word= word.replace("%C3%B3", "o")
            word= word.replace("%C3%BA", "u")
            word= word.replace("%C3%B1", "ñ")
        return word 
    
class UserCountryCity(object):
    
    def __init__(self):
        self.acces_token = "293167064069597|D_XAWchMl1H8rLHjgKvDVwwZCig"
        self.api = Api(self.acces_token)    
        self.obj = UserDataIE()
    
    def analize_pages(self):
        MAX_NUMBER_TAB = 30
        MAX_RESQUEST_HOUR = 1250
        finish_time = datetime.now() + timedelta(hours=1)
        page_list = db.stored_procedure(db.STP_SEL_PAGINA_SIN_REGION)
        print("Por actualizar %s paginas" % len(page_list))
        request = 0
        number_tab = 0
        for page in page_list:
            self.save_data(page)
            request += 1
            number_tab += 1
            if number_tab >= MAX_NUMBER_TAB:
                self.release_memory()
                number_tab = 0
            if request >= MAX_RESQUEST_HOUR:
                remaining_time = self.remaining_seconds(finish_time, datetime.now())
                if remaining_time > 0: # faltaba tiempo para cumplir el limite de request por hora
                    print("Durmiendo %s segundos (se realizo %s requests por hora)" % (remaining_time, MAX_RESQUEST_HOUR))
                    sleep(remaining_time)
                request = 0
                finish_time = datetime.now() + timedelta(hours=1)
   
    def save_data(self, page):
        url = "http://www.facebook.com/"+page.ds_pagina
        id_fb_location, location, friends = self.obj.get_user_data_facebook(url)
        if id_fb_location:
            if db.stored_procedure(db.STP_SEL_ESNUEVA_REGION, page.id_pagina, id_fb_location, location, friends)[0].nueva == 0:
                fb_page = self.api.parse_user(id_fb_location)
                if 'location' in fb_page:
                    country, city = self.get_city_country(fb_page['location']['latitude'], fb_page['location']['longitude'], "lindexa27")
                    db.stored_procedure(db.STP_UPD_REGION_SEGUIDORES, page.id_pagina, id_fb_location, location, friends, city, country)
        else:
            db.stored_procedure(db.STP_UPD_REGION_SEGUIDORES, page.id_pagina, None, None, friends, None, None)
        

    def get_city_country(self, latitude, longitude, user_name):
        while True:
            msg = backoff("http://api.geonames.org/countrySubdivisionJSON?lat=%s&lng=%s&username=%s" % (latitude, longitude, user_name))
            response = json.loads(msg)
            if 'status' in response:
                value =  response['status']['value']
                if value != 18 and value != 19 and value != 20:
                    break
                print("Durmiendo 10 minutos")
                sleep(600)
            else:
                break
            
        country = None
        city = None
        if 'countryName' in response:
            country = response['countryName']
        if 'adminName1' in response:
            city = response['adminName1']
            
        return country, city
     
    def remaining_seconds(self, target, now):
        delta = (target - now)
        return delta.days*86400 + delta.seconds 
    
    def release_memory(self):
        print("Liberando memoria del Internet Explorer")
        os.system("taskkill /im iexplore.exe /f")
        try:
            os.system("taskkill /im WerFault.exe /f")
        except Exception as e:
            print(e)
        sleep(10)
        
if __name__ == '__main__':
    obj = UserCountryCity()
    obj.analize_pages()
        
#    id_list = ["100001346455303","nicolasmendozadelsolar","sharhorodska","rlopezc27", "arnoldrafael.gonzalez", "bertonrony.soriarioja"]
#    for id1 in id_list:
#        url = "http://www.facebook.com/"+id1
#        idd, location, friends = obj.get_user_data_facebook(url)
#        print(idd, location, friends)
   
#    os.system("taskkill /im iexplore.exe /f")
