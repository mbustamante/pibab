# -*- coding: utf-8 -*-
'''
Created on 02/08/2012
@author: Roque Lopez
@contact: rlopezc27@gmail.com
'''
import smtplib
import config.data_base as db
from email.header import Header
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email import charset
from config.utils import normalize_string


class Sender(object):
    
    def __init__(self, account, password, server, port=587):
        self.account = account
        self.password = password
        self.mailServer = smtplib.SMTP(server)
        self.mailServer.ehlo()
        self.mailServer.starttls()
        self.mailServer.ehlo()
        self.mailServer.login(account, password)       
        
    def send_mail(self, subject, html, emails):
        charset.CHARSETS['utf-8'] = ( charset.QP, charset.QP, 'utf-8' )
        msg = MIMEMultipart('alternative')
        try:       
            msg['Subject'] = Header(subject, 'UTF-8').encode()
            msg['From'] = Header(self.account, 'UTF-8').encode()  
            msg['To'] = Header(", ".join(emails), 'UTF-8').encode() 
            part1 = MIMEText(html, 'html', 'UTF-8')
        except:
            msg['Subject'] = Header(normalize_string(subject), 'UTF-8').encode()
            msg['From'] = Header(self.account, 'UTF-8').encode()   
            msg['To'] = Header(normalize_string(", ".join(emails)), 'UTF-8').encode() 
            part1 = MIMEText(normalize_string(html), 'html', 'UTF-8')
        msg.attach(part1)
        self.mailServer.sendmail(self.account, emails, msg.as_string())
        
    def close(self):
        self.mailServer.quit()


class HTML_Creator(object):
    
    def __init__(self):
        pass
    
    def create_alert(self, comment, url):
        body = """\
        <html>
          <head></head>
          <body>
            <p><em>Estimado</em>,<br/>
               El siguiente comentario ha producido una alarma<br>
               <strong>"%s"</strong><br/>
               Para ver el comentario visite este <a href="%s">link</a>.<br/><br/>
               <em>Equipo Lindexa</em>
            </p>
          </body>
        </html>""" % (comment, url)
        return body
    
    def create_summary(self, id_summary, summary_type):
        body = """
        <html>
            <head></head>
            <body>
                <table  border="0">
                  <tr>
                    <td>Estimado, le enviamos el <strong>Resumen %s</strong>.<br/></td>
                  </tr>
                  <tr>
                    <td align="center"><strong>Estadísticas</strong></td>
                  </tr>
                  <tr>
                    <td align="center">%s <br/></td>
                  </tr>
                  <tr>
                    <td align="center"><strong>Resumen Generado</strong></td>
                  </tr>
                  <tr>
                    <td>%s <br/></td>
                  </tr>
                  <tr>
                    <td>Para mayor información visite este <a href="http://lindexa-app.cloudapp.net/Login.aspx">link</a><br/><br/><em>Equipo Lindexa</em></td>
                  </tr>
                </table>
            </body>
        </html>""" % (summary_type, self.create_frame(id_summary), self.create_table(id_summary))         
        return body  
 
    def create_table(self, id_summary_master):
        data_master = db.stored_procedure(db.STP_SEL_RESUMEN_MAESTRO, id_summary_master)
        header = """
        <table border="1">
          <tr BGCOLOR="#999999">
            <td><strong>Fuente</strong></td>
            <td><strong>Cuenta/Medio</strong></td>
            <td><strong>Fecha y Hora</strong></td>
            <td><strong>Texto</strong></td>
            <td><strong>Sentimiento</strong></td>
            <td><strong>Impacto</strong></td>
            <td><strong>Link</strong></td>
            <td></td>
          </tr>"""
        
        body = ""
        for master in data_master:
            body += """
            <tr BGCOLOR="#CCCCCC">
                <td>%s</td>
                <td>%s</td>
                <td>%s</td>
                <td>%s</td>
                <td>%s</td>
                <td>%s</td>
                <td><a href="%s">Link</a></td>
                <td>%s</td>
            </tr>""" % (master.tp_fuente, master.nombre_cuenta, master.fecha_hora, master.resumen or "", master.sentimiento, master.impacto, master.link, "")

            data_detail = db.stored_procedure(db.STP_SEL_RESUMEN_DETALLE, master.id_comentario, master.id_resumen_Cab)
            for detail in data_detail:
                body += """
                <tr>
                    <td align="center">&#8594;</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td><a href="%s">Link</a></td>
                </tr>""" % ( detail.tp_fuente, detail.nombre_cuenta, detail.fecha_hora, detail.resumen or "", detail.sentimiento, detail.impacto, detail.link)
        return header + body + "</table>"
    
    def create_frame(self, id_summary_frame):
        data = db.stored_procedure(db.STP_SEL_RESUMEN_CUADRO, id_summary_frame)
        header = """
        <table width="100" border="1">
          <tr BGCOLOR="#999999">
            <td><strong>Fuente</strong></td>
            <td><strong>Positivos</strong></td>
            <td><strong>Negativos</strong></td>
            <td><strong>Neutros</strong></td>
            <td><strong>Total</strong></td>
          </tr>"""
        
        body = ""
        for row in data:
            body += """
            <tr>
                <td>%s</td>
                <td>%s</td>
                <td>%s</td>
                <td>%s</td>
                <td>%s</td>
            </tr>""" % (row.tp_fuente_resumen, row.num_positivo, row.num_negativo, row.num_neutro, row.total)
        return header + body + "</table>"
    
if __name__ == '__main__':
    obj = Sender("lindexa.soporte@gmail.com","ladrillo27", "smtp.gmail.com")
    my_list = ["rlopezc27@gmail.com", "puxama@gmail.com", "marunvn@gmail.com"]
    obj.send_mail("Alarma - Lindexa producido por Movistar Perú Willax Televisión (1023006 seguidores) en Facebook", "¡Cuando es más fácil!  ñ",my_list)
    obj.close()
    