# -*- coding: utf-8 -*-
'''
Created on 19/04/2012
@author: Roque Lopez
@contact: rlopezc27@gmail.com
'''
import threading
import config.data_base as db
from time import sleep
from datetime import datetime, timedelta
from config.utils import TIME_DIFF

class Manager(threading.Thread):
    ''' Clase encargada  del control y gestion de los hilos '''

    def __init__(self, interval=5):
        threading.Thread.__init__(self)
        self.__update_interval = interval * 60
        self.__demon = Demon_SP()
    
    def run(self):
        ''' Bucle infinito '''  
        self.__demon.start()          
        while True:
            sleep(self.__update_interval)
            self.__demon.update_data()

                
class Demon_SP(threading.Thread):
    
    def __init__(self):
        threading.Thread.__init__(self)
        self.update_data()
       
    def update_data(self):
        print("Actualizando datos")
        self.__sorted_list = db.stored_procedure(db.STP_SEL_HORAS_ACTUALIZACION)
          
    def run(self):
        while True:
            remaining_time = self.remaining_time()
            self.sleeping(remaining_time)
            if remaining_time > 60:
                self.execute_sp()
    
    def remaining_time(self):
        print("Calculando proximo lanzamiento...")
        now = datetime.now() #+ timedelta(seconds=TIME_DIFF)
        now_seconds = now.hour*3600 + now.minute*60 + now.second
        for data in self.__sorted_list:
            target_seconds = data.fecha.hour*3600 + data.fecha.minute*60 + data.fecha.second   
            if target_seconds > now_seconds:
                return target_seconds - now_seconds 
        # El porximo lanzamiento sera al siguiente dia    
        target = datetime(now.year, now.month, now.day, self.__sorted_list[0].fecha.hour, self.__sorted_list[0].fecha.minute, self.__sorted_list[0].fecha.second) + timedelta(days=1)        
        return self.remaining_seconds(target, now)
        
    def remaining_seconds(self, target, now):
        delta = (target - now)
        return delta.days*86400 + delta.seconds
        
    def sleeping(self, seconds):
        try:
            print("info, Sleeping '%s' segundos" % (seconds))
            sleep(seconds)
        except Exception as e:
            print(e)
            
    def execute_sp(self):
        print("info, Actualizando el Modulo Analisis")
        db.stored_procedure(db.STP_UPD_TODO_ANALISIS)       
                               
if __name__ == '__main__':
    obj = Manager(1)
    obj.start()

                