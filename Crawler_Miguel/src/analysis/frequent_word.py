# -*- coding: utf-8 -*-
'''
Created on Feb 22, 2012
@author: Roque Lopez
@contact: rlopezc27@gmail.com
'''
import re
from config.settings import SPLITTER_FREQUENT_WORD as SPLITTER

class MFS (object):
    
    def __init__(self, text, threshold):
        self.__threshold = threshold
        self.__mfs_list = dict()
        self.__frequency_list = dict()
        self.__words = list()
        self.__get_mfs(text)
    
    def __get_mfs(self, text):
        self.__get_frequency_list(text)
        size = len(self.__words)
        word = ""
        next_word = ""
        frequency = 0
        position = 0
        occurrence_list = list()
        while position < size:
            word = self.__words[position]
            frequency = self.__frequency_list[word].get_frequency()
            occurrence_list = self.__frequency_list[word].get_position_list()
            ngrama_size = 0
            final_frequency = 0
            ngrama = list()
            while frequency >= self.__threshold and word != SPLITTER and word != "":
                final_frequency = frequency
                ngrama.append(word)
                ngrama_size = len(ngrama)
                if position+ngrama_size >= size:
                    break
                next_word = self.__words[position+ngrama_size]
                occurrence_list = self.__update_occurrence_list(occurrence_list, ngrama_size, next_word)
                word = next_word
                frequency = len(occurrence_list)
            
            if ngrama_size > 1:
                self.__add_mfs(ngrama, final_frequency)
            position += 1
    
    def __update_occurrence_list(self, occurrence_list, ngrama_size, next_word):
        size = len(self.__words)  
        size_list = len(occurrence_list)
        new_occurence_list = list()
        i = 0
        while i < size_list:
            position = occurrence_list[i]
            if position+ngrama_size >= size:
                    break
            if next_word == self.__words[position+ngrama_size]:
                new_occurence_list.append(position)
            i += 1
        return new_occurence_list
    
    def __add_mfs(self, ngrama, frequency):
        posible_mfs = " ".join(ngrama)
        #print (posible_mfs, str(frequency))
        if posible_mfs in self.__mfs_list:
            return
        for mfs in list(self.__mfs_list.keys()):
            if mfs.count(posible_mfs) > 0:
                return
            if posible_mfs.count(mfs) > 0:
                del self.__mfs_list[mfs]
        self.__mfs_list[posible_mfs] = frequency
        
    def __get_frequency_list(self, text):
        self.__words = re.split("\s+", text) # Divide el texto por cualquier caracter en blanco 
        position = 0
        for word in self.__words:
            if word in self.__frequency_list:
                self.__frequency_list[word].add_position(position)
            else:
                obj_word = Word(word, position)
                self.__frequency_list[word] = obj_word
            position += 1
   
    def get_top(self, top):
        size = len(self.__mfs_list)
        top_list = list()
        if top > size:
            top = size
        sorted_list = sorted(self.__mfs_list.items(), key=lambda x:x[1], reverse=True)
        i = 0
        while i < top:
            print (sorted_list[i])
            top_list.append(sorted_list[i])
            i += 1
        return top_list
    
class NGrama (object):
    
    def __init__(self, text, threshold, length):
        self.__threshold = threshold
        self.__length = length
        self.__ngrama_list = dict()
        self.__frequency_list = dict()
        self.__words = list()
        self.__get_ngrama(text)
    
    def __get_ngrama(self, text):
        self.__get_frequency_list(text)
        size = len(self.__words)
        word = ""
        next_word = ""
        frequency = 0
        position = 0
        occurrence_list = list()
        while position < size:
            word = self.__words[position]
            frequency = self.__frequency_list[word].get_frequency()
            occurrence_list = self.__frequency_list[word].get_position_list()
            ngrama_size = 0
            final_frequency = 0
            ngrama = list()
            while frequency >= self.__threshold and word != SPLITTER and word != "":
                final_frequency = frequency
                ngrama.append(word)
                ngrama_size = len(ngrama)
                if ngrama_size == self.__length: break
                if position+ngrama_size >= size:
                    break
                next_word = self.__words[position+ngrama_size]
                occurrence_list = self.__update_occurrence_list(occurrence_list, ngrama_size, next_word)
                word = next_word
                frequency = len(occurrence_list)
            
            if ngrama_size == self.__length:
                self.__add_ngrama(ngrama, final_frequency)
            position += 1
    
    def __update_occurrence_list(self, occurrence_list, ngrama_size, next_word):
        size = len(self.__words)  
        size_list = len(occurrence_list)
        new_occurence_list = list()
        i = 0
        while i < size_list:
            position = occurrence_list[i]
            if position+ngrama_size >= size:
                    break
            if next_word == self.__words[position+ngrama_size]:
                new_occurence_list.append(position)
            i += 1
        return new_occurence_list
    
    def __add_ngrama(self, ngrama, frequency):
        posible_ngrama = " ".join(ngrama)
        #print (posible_ngrama, str(frequency))
        if posible_ngrama in self.__ngrama_list:
            return 
        self.__ngrama_list[posible_ngrama] = frequency

    def __get_frequency_list(self, text):
        self.__words = re.split("\s+", text) # Divide el texto por cualquier caracter en blanco
        position = 0
        for word in self.__words:
            if word in self.__frequency_list:
                self.__frequency_list[word].add_position(position)
            else:
                obj_word = Word(word, position)
                self.__frequency_list[word] = obj_word
            position += 1
        
    def get_top(self, top):
        size = len(self.__ngrama_list)
        top_list = list()
        if top > size: top = size
        sorted_list = sorted(self.__ngrama_list.items(), key=lambda x:x[1], reverse=True)
        i = 0
        while i < top:
            print (sorted_list[i])
            top_list.append(sorted_list[i])
            i += 1
        return top_list
    
class Word (object):
    
    def __init__(self, word, position):
        self.__word = word
        self.__position_list = list()
        self.__position_list.append(position)
        
    def add_position(self, position):
        self.__position_list.append(position)
    
    def get_position_list(self):
        return self.__position_list
    
    def get_frequency(self):
        return len(self.__position_list)
    
    def get_word(self):
        return self.__word
        
if __name__ == '__main__':
    import time
    print ("Starting... ",time.strftime("%H:%M:%S %d %b"))
    print ("Cleaning Text...")
    my_file = open("../resource/mfs.txt",'r',encoding='utf-8');
    text = my_file.read()
    my_file.close()
    print ("Evaluating...")
    tmp = MFS(text, 2)
    tmp_list = tmp.get_top(7)
    
    print ("Finished... ",time.strftime("%H:%M:%S %d %b"))