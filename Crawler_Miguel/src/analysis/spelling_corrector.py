# -*- coding: utf-8 -*-
'''
Created on Feb 9, 2012
@author: Roque Lopez
@contact: rlopezc27@gmail.com
'''
from collections import defaultdict
import re
import codecs

class Corrector (object):
    
    def __init__(self, file_path):
        self.__words = self.__train(self.__words(codecs.open(file_path,'r',encoding='utf-8').read()))
        self.__alphabet = 'abcdefghijklmnopqrstuvwxyz'
               
    def __words(self,text): 
        return re.findall("[a-z\xE1\xE9\xED\xF3\xFA\xF1]+", text)   
  
    def __train(self,features):
        model = defaultdict(lambda: 1)
        for f in features:
            model[f] += 1
        return model  
  
    def __edits1(self,word):
        s = [(word[:i], word[i:]) for i in range(len(word) + 1)]
        deletes    = [a + b[1:] for a, b in s if b]
        transposes = [a + b[1] + b[0] + b[2:] for a, b in s if len(b)>1]
        replaces   = [a + c + b[1:] for a, b in s for c in self.__alphabet if b]
        inserts    = [a + c + b     for a, b in s for c in self.__alphabet]
        return set(deletes + transposes + replaces + inserts)  
  
    def __known_edits2(self,word):  
        return set(e2 for e1 in self.__edits1(word) for e2 in self.__edits1(e1) if e2 in self.__words)  
  
    def __known(self,words): 
        return set(w for w in words if w in self.__words)  
  
    def correct(self,word):  
        candidates = self.__known([word]) or self.__known(self.__edits1(word)) or self.__known_edits2(word) or [word]  
        return max(candidates, key=self.__words.get)
        