# -*- coding: utf-8 -*-
'''
Created on Feb 21, 2012
@author: Roque Lopez
@contact: rlopezc27@gmail.com
'''
from analysis.classifier import Classifier
import time

if __name__ == '__main__':
    print ("Starting... ",time.strftime("%H:%M:%S %d %b"))
    folder = "../resource/entrada/"
    size = 1
    i = 1
    print ("word +,word -,emoticon +,emoticon -,combination +,combination -,num word +,num word -,num emoticon +,num emoticon -,num combintion +,num combintion -")
    while i <= size:
        #file_name = str(i)+".txt"
        #print(file_name)
        #file = open(folder+file_name, 'r', encoding='utf-8')
        comment = 'TE ODIO MOVISTAR  --> CHORISTAR PAGAS MAS & TE ROBAN MAS  // TELEFONICA  --> CHORIFONICA , PAGAS PUNTUAL Y TE  VUELVEN MAS LENTA LA LINEA'
        obj = Classifier()
        obj.classify(comment)
        print(obj.get_contribution_positive(),obj.get_contribution_negative(), obj.get_score())
        i += 1
    print ("Finished... ",time.strftime("%H:%M:%S %d %b"))

