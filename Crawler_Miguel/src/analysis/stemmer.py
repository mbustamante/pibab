# -*- coding: utf-8 -*-
'''
Created on Feb 20, 2012
@author: Roque Lopez
@contact: rlopezc27@gmail.com
'''

class SpanishStemmer (object):
    
    def __init__(self):
        self.__vowels = "aeiou\xE1\xE9\xED\xF3\xFA\xFC"
        self.__step0_suffixes = ("selas", "selos", "sela", "selo", "las",
                            "les", "los", "nos", "me", "se", "la", "le", 
                            "lo")
        self.__step1_suffixes = ("amientos", "imientos", "amiento", "imiento",
                            "aciones", "uciones", "adoras", "adores",
                            "ancias", "log\xEDas", "encias", "amente",
                            "idades", "anzas", "ismos", "ables", "ibles",
                            "istas", "adora", "aci\xF3n", "antes", 
                            "ancia", "log\xEDa", "uci\xf3n", "encia", 
                            "mente", "anza", "icos", "icas", "ismo", 
                            "able", "ible", "ista", "osos", "osas", 
                            "ador", "ante", "idad", "ivas", "ivos", 
                            "ico",
                            "ica", "oso", "osa", "iva", "ivo")
        self.__step2a_suffixes = ("yeron", "yendo", "yamos", "yais", "yan", 
                             "yen", "yas", "yes", "ya", "ye", "yo", 
                             "y\xF3")
        self.__step2b_suffixes = ("ar\xEDamos", "er\xEDamos", "ir\xEDamos",
                             "i\xE9ramos", "i\xE9semos", "ar\xEDais",
                             "aremos", "er\xEDais", "eremos", 
                             "ir\xEDais", "iremos", "ierais", "ieseis", 
                             "asteis", "isteis", "\xE1bamos", 
                             "\xE1ramos", "\xE1semos", "ar\xEDan", 
                             "ar\xEDas", "ar\xE9is", "er\xEDan", 
                             "er\xEDas", "er\xE9is", "ir\xEDan", 
                             "ir\xEDas", "ir\xE9is",
                             "ieran", "iesen", "ieron", "iendo", "ieras",
                             "ieses", "abais", "arais", "aseis", 
                             "\xE9amos", "ar\xE1n", "ar\xE1s", 
                             "ar\xEDa", "er\xE1n", "er\xE1s", 
                             "er\xEDa", "ir\xE1n", "ir\xE1s",
                             "ir\xEDa", "iera", "iese", "aste", "iste",
                             "aban", "aran", "asen", "aron", "ando", 
                             "abas", "adas", "idas", "aras", "ases", 
                             "\xEDais", "ados", "idos", "amos", "imos", 
                             "emos", "ar\xE1", "ar\xE9", "er\xE1", 
                             "er\xE9", "ir\xE1", "ir\xE9", "aba", 
                             "ada", "ida", "ara", "ase", "\xEDan", 
                             "ado", "ido", "\xEDas", "\xE1is", 
                             "\xE9is", "\xEDa", "ad", "ed", "id", 
                             "an", "i\xF3", "ar", "er", "ir", "as", 
                             "\xEDs", "en", "es")
        self.__step3_suffixes = ("os", "a", "e", "o", "\xE1",
                            "\xE9", "\xED", "\xF3")
    
    def stem(self, word):
        """
        Stem a Spanish word and return the stemmed form.
        
        @param word: The word that is stemmed.
        @type word: C{str, unicode}
        @return: The stemmed form.
        @rtype: C{unicode}
        
        """
        word = word.lower()
               
        step1_success = False

        r1, r2 = self.__r1r2_standard(word, self.__vowels)
        rv = self.__rv_standard(word, self.__vowels)

        # STEP 0: Attached pronoun
        for suffix in self.__step0_suffixes:
            if word.endswith(suffix):
                if rv.endswith(suffix):
                    if rv[:-len(suffix)].endswith(("i\xE9ndo", 
                                                   "\xE1ndo",
                                                   "\xE1r", "\xE9r",
                                                   "\xEDr")):
                        word = (word[:-len(suffix)].replace("\xE1", "a")
                                                   .replace("\xE9", "e")
                                                   .replace("\xED", "i"))
                        r1 = (r1[:-len(suffix)].replace("\xE1", "a")
                                               .replace("\xE9", "e")
                                               .replace("\xED", "i"))
                        r2 = (r2[:-len(suffix)].replace("\xE1", "a")
                                               .replace("\xE9", "e")
                                               .replace("\xED", "i"))
                        rv = (rv[:-len(suffix)].replace("\xE1", "a")
                                               .replace("\xE9", "e")
                                               .replace("\xED", "i"))

                    elif rv[:-len(suffix)].endswith(("ando", "iendo",
                                                     "ar", "er", "ir")):
                        word = word[:-len(suffix)]
                        r1 = r1[:-len(suffix)]
                        r2 = r2[:-len(suffix)]
                        rv = rv[:-len(suffix)]

                    elif (rv[:-len(suffix)].endswith("yendo") and
                          word[:-len(suffix)].endswith("uyendo")):
                        word = word[:-len(suffix)]
                        r1 = r1[:-len(suffix)]
                        r2 = r2[:-len(suffix)]
                        rv = rv[:-len(suffix)]
                break

        # STEP 1: Standard suffix removal
        for suffix in self.__step1_suffixes:
            if word.endswith(suffix):
                if suffix == "amente" and r1.endswith(suffix):
                    step1_success = True
                    word = word[:-6]
                    r2 = r2[:-6]
                    rv = rv[:-6]

                    if r2.endswith("iv"):
                        word = word[:-2]
                        r2 = r2[:-2]
                        rv = rv[:-2]

                        if r2.endswith("at"):
                            word = word[:-2]
                            rv = rv[:-2]

                    elif r2.endswith(("os", "ic", "ad")):
                        word = word[:-2]
                        rv = rv[:-2]

                elif r2.endswith(suffix):
                    step1_success = True
                    if suffix in ("adora", "ador", "aci\xF3n", "adoras",
                                  "adores", "aciones", "ante", "antes",
                                  "ancia", "ancias"):
                        word = word[:-len(suffix)]
                        r2 = r2[:-len(suffix)]
                        rv = rv[:-len(suffix)]

                        if r2.endswith("ic"):
                            word = word[:-2]
                            rv = rv[:-2]

                    elif suffix in ("log\xEDa", "log\xEDas"):
                        word = word.replace(suffix, "log")
                        rv = rv.replace(suffix, "log")

                    elif suffix in ("uci\xF3n", "uciones"):
                        word = word.replace(suffix, "")
                        rv = rv.replace(suffix, "")

                    elif suffix in ("encia", "encias"):
                        word = word.replace(suffix, "ente")
                        rv = rv.replace(suffix, "ente")

                    elif suffix == "mente":
                        word = word[:-5]
                        r2 = r2[:-5]
                        rv = rv[:-5]

                        if r2.endswith(("ante", "able", "ible")):
                            word = word[:-4]
                            rv = rv[:-4]

                    elif suffix in ("idad", "idades"):
                        word = word[:-len(suffix)]
                        r2 = r2[:-len(suffix)]
                        rv = rv[:-len(suffix)]

                        for pre_suff in ("abil", "ic", "iv"):
                            if r2.endswith(pre_suff):
                                word = word[:-len(pre_suff)]
                                rv = rv[:-len(pre_suff)]

                    elif suffix in ("ivo", "iva", "ivos", "ivas"):
                        word = word[:-len(suffix)]
                        r2 = r2[:-len(suffix)]
                        rv = rv[:-len(suffix)]
                        if r2.endswith("at"):
                            word = word[:-2]
                            rv = rv[:-2]
                    else:
                        word = word[:-len(suffix)]
                        rv = rv[:-len(suffix)]
                break

        # STEP 2a: Verb suffixes beginning "y"
        if not step1_success:
            for suffix in self.__step2a_suffixes:
                if (rv.endswith(suffix) and
                    word[-len(suffix)-1:-len(suffix)] == ""):
                    word = word[:-len(suffix)]
                    rv = rv[:-len(suffix)]
                    break

        # STEP 2b: Other verb suffixes
            for suffix in self.__step2b_suffixes:
                if rv.endswith(suffix):
                    if suffix in ("en", "es", "\xE9is", "emos"):
                        word = word[:-len(suffix)]
                        rv = rv[:-len(suffix)]

                        if word.endswith("g"):
                            word = word[:-1]

                        if rv.endswith("g"):
                            rv = rv[:-1]
                    else:
                        word = word[:-len(suffix)]
                        rv = rv[:-len(suffix)]
                    break

        # STEP 3: Residual suffix
        for suffix in self.__step3_suffixes:
            if rv.endswith(suffix):
                if suffix in ("e", "\xE9"):
                    word = word[:-len(suffix)]
                    rv = rv[:-len(suffix)]

                    if word[-2:] == "g" and rv[-1] == "":
                        word = word[:-1]
                else:
                    word = word[:-len(suffix)]
                break

        word = (word.replace("\xE1", "a").replace("\xE9", "e")
                    .replace("\xED", "i").replace("\xF3", "o")
                    .replace("\xFA", ""))
        return word
    
    def __r1r2_standard(self, word, vowels):
        r1 = ""
        r2 = "" 
        for i in range(1, len(word)):
            if word[i] not in vowels and word[i-1] in vowels:
                r1 = word[i+1:]
                break

        for i in range(1, len(r1)):
            if r1[i] not in vowels and r1[i-1] in vowels:
                r2 = r1[i+1:]
                break

        return (r1, r2)
    
    def __rv_standard(self, word, vowels):
        rv = ""
        if len(word) >= 2:
            if word[1] not in vowels:
                for i in range(2, len(word)):
                    if word[i] in vowels:
                        rv = word[i+1:]
                        break
            elif word[:2] in vowels:
                for i in range(2, len(word)):
                    if word[i] not in vowels:
                        rv = word[i+1:]
                        break
            else:
                rv = word[3:]
        return rv
   
if __name__ == '__main__':
    word_list = ['penales','internet','android','movistar','celular','web','blackberry','pagina web','youtube','telefonia fija','iphone','smartphone','cable','inter','tv','canal','fija','RATAFONICA','pc','móvil','laptop','celu','trio','rpm','usb','face','cmd','duo','tele','lcd','cable magico','speedy','fonoya','timofonica','aishuna','larga distancia','robofonica','robistar','choristar','telefono fijo','larga distancia fijo ','línea fija ','llamadas fijo','casa fijo','interferencia fijo','instalación fijo','comunicación fijo','amper fijo ','beetel fijo','inalámbrico fijo','buzon voz fijo','identificador fijo','conferencia fijo','desvio fijo','tarjeta fijo','control fijo','telefonia movil','recargas movil','saldo movil','llamadas movil','caída movil ','señal movil','equipo movil ','planes movil','apaga movil','servicio tecnico movil','roaming ','mensajes movil','portabilidad movil','lento internet','speedy movil','lag ','navegar ','compu ','computadora ','velocidad internet','uno internet','televisión ','señal cable','canales ','borrosos cable ','deco ','decodificador ','duna ','premium cable','analógico cable','video ','satelital cable','digital cable','#NoAlPeorServiciodeInternet','#Estafafónica','#Choristar','#Estafadores','#Robo','#Ratas','#Rateros','#robame','#basuras','#speedy','#abuso','#anonymous','#RPM','#robistar','amper']
    obj = SpanishStemmer()
    for word in word_list:
        print (obj.stem(word))