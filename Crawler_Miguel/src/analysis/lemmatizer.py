# -*- coding: utf-8 -*-
'''
Created on 02/02/2012
@author: Roque Lopez
@contact: rlopezc27@gmail.com
'''
import analysis.corpus as corpus

class Stemmer (object):
       
    def __init__(self,file_path):
        self.__list = corpus.stemm_list(file_path)
        
    def get_stem(self,word):
        if word in self.__list:
            return self.__list.get(word)
        else:
            return word