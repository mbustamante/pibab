# -*- coding: utf-8 -*-
'''
Created on Feb 8, 2012
@author: eLRuLL, Roque Lopez
@contact: rlopezc27@gmail.com
'''

import json
from crawler_facebook.error import backoff
from config.utils import urlcreator

class Api(object):
    ''' Clase que implementa los metodos necesarios para crawlear FB para Lindexa'''
    
    def __init__(self, access_token=''):        
        self.__attemps = 8
        self.__access_token = r'access_token=' + access_token
        self.__api_url = 'graph.facebook.com'
                   
    def get_access_token(self):
        return self.__access_token
    
    def set_access_token(self,access_token):
        self.__access_token = access_token
    
    def parse_publication(self, fbid, since='', until='', limit=25, locale='es_LA'):
        ''' Obtiene informacion de una publicacion 
            fbid : Facebook ID of the user we want to parse
            since : Fecha desde donde crawlear(usar unixtime) (OPTIONAL)
            until : Fecha hasta donde crawlear(usar unixtime) (OPTIONAL)
            limit : Cuantas publicaciones obtener por consulta. Aprox. maximo es 5000 (OPTIONAL)
        '''
        params = [self.__access_token, 'limit=' + str(limit), 'locale=' + locale]
        if since != '':
            params.append('since=' + since)  
        if until != '':
            params.append('until=' + until)
            
        completeurl = urlcreator('https', self.__api_url, fbid + r'/feed', params)       
        #print(completeurl)
        msg = backoff(completeurl, self.__attemps)
        response = json.loads(msg)
        data= {} # objeto que contendra la 'data' y el 'paging'
        data['data'] = []
        if 'data' in response:
            x = 0
            for i in response['data']:
                data['data'].append({})
                data['data'][x]['id'] = i['id']
                data['data'][x]['from'] = i['from']['id']
                data['data'][x]['from_name'] = i['from']['name']
                data['data'][x]['type'] = i['type']
                data['data'][x]['time'] = i['created_time']
                data['data'][x]['comments'] = i['comments']['count']
                data['data'][x]['link'] = r'http://www.facebook.com/%s/posts/%s' % (i['id'].split('_')[0], i['id'].split('_')[1])
                                
                if 'likes' in i:
                    data['data'][x]['likes'] = i['likes']['count']
                else:
                    data['data'][x]['likes'] = 0
                    
                if 'shares' in i:
                    data['data'][x]['shares'] = i['shares']['count']
                else:
                    data['data'][x]['shares'] = 0
                
                if 'message' in i:
                    data['data'][x]['message'] = i['message']
                else:
                    if i['type'] == 'photo':
                        data['data'][x]['message'] = i['caption'] if 'caption' in i else i['name']
                    elif i['type'] == 'video' or i['type'] == 'link':
                        data['data'][x]['message'] = i['name']
                    elif i['type'] == 'question':
                        data['data'][x]['message'] = i['story']
                    else:
                        data['data'][x]['message'] = i['type']
                
                x += 1
                
        if 'paging' in response: # Hay mas publicaciones?
            data['paging'] = response['paging']
        else:
            data['paging'] = {}
        return data
        
    def parse_comment(self, pubid, after_id='', limit=50, locale='es_LA'):
        ''' Obtiene informacion de los comentarios de una publicacion
            pubid : ID de una Facebook Publication
            limit : Cuantas comentarios obtener por consulta. Aprox. maximo es 5000 (OPTIONAL)
        '''
        params = [self.__access_token, 'limit=' + str(limit), 'locale=' + locale]
        if after_id != '':
            params.append('__after_id=' + after_id)      
              
        completeurl = urlcreator('https', self.__api_url, pubid + r'/comments', params)
        #print(completeurl)
        msg = backoff(completeurl, self.__attemps)
        response = json.loads(msg)
        data = {} # objeto que contendra la 'data' y el 'paging'
        data['data'] = []
        if 'data' in response:
            x = 0
            for i in response['data']:
                data['data'].append({})
                data['data'][x]['id'] = i['id']
                data['data'][x]['from'] = i['from']['id']
                data['data'][x]['from_name'] = i['from']['name']
                data['data'][x]['type'] = 'comment'
                data['data'][x]['time'] = i['created_time']
                data['data'][x]['likes'] = i['like_count']
                data['data'][x]['comments'] = None
                data['data'][x]['link'] = r'http://www.facebook.com/%s/posts/%s' % (i['id'].split('_')[0], i['id'].split('_')[1])
                
                if 'message' in i:
                    data['data'][x]['message'] = i['message']
                else:
                    if i['type'] == 'photo':
                        data['data'][x]['message'] = i['caption'] if 'caption' in i else i['name']
                    elif i['type'] == 'video' or i['type'] == 'link':
                        data['data'][x]['message'] = i['name']
                    elif i['type'] == 'question':
                        data['data'][x]['message'] = i['story']
                    else:
                        data['data'][x]['message'] = i['type']
                x += 1
        
        if 'paging' in response: # Hay mas comentarios?
            data['paging'] = response['paging']
        else:
            data['paging'] = {}
        return data
         
    def parse_user(self, userid):
        ''' Obtiene informacion de un usuario 
            userid : Facebook User ID
        '''
        msg = backoff(r'http://graph.facebook.com/' + userid, self.__attemps)
        response = json.loads(msg)
        if response == False: # cambiar esta condicion
            return {'id':-1}
        return response
    
    def parse_publication_by_id(self, fbid):
        ''' Obtiene informacion de una publicacion especifica '''
        params = [self.__access_token]           
        completeurl = urlcreator('https', self.__api_url, fbid, params) 
        print(completeurl)      
        msg = backoff(completeurl, self.__attemps)
        response = json.loads(msg)
        data = {}
        #falta corregir, solo recupera 25 likes y 25 comentarios como maximo
        if 'comments' in response:
            data['comments'] = len(response['comments']['data'])
        else:
            data['comments'] = 0
        if 'likes' in response:
            data['likes'] = len(response['likes']['data'])
        else:
            data['likes'] = 0
        return data                      
                
    def batch_request(self, json_array):
        ''' Batch Request para Usuarios de Facebook '''
        
        # json_array : Array formateado en Json donde se contienen todas las sentencias que se hara en un batch Request 
        msg = backoff(r'https://' + self.__api_url + r'/?batch=' + json.dumps(json_array) + r'&' + self.__access_token + r'&method=post',self.__attemps)
        
        response = json.loads(msg)
        res = []
        for i in range(len(response)):
            res[i] = {}
            if response[i]['code'] == 200:
                res[i]['id'] = response[i]['body']['id']
                res[i]['name'] = response[i]['body']['name']
                res[i]['gender'] = response[i]['body']['gender']
                res[i]['locale'] = response[i]['body']['locale']
        return res
    
    def search_post(self, keyword, type_q='and', since='', until='', limit=25, locale='es_LA'):
        ''' Busca keywords en post publicos. Si es un keyword de mas de una palabra, utiliza 
            el operador AND por defecto
        '''
        keyword_list = keyword.split(" ")
        params = [self.__access_token,'type=post', 'limit=' + str(limit), 'locale='+locale]
        if since != '':
            params.append('since=' + since)  
        if until != '':
            params.append('until=' + until) 
        if type_q == 'and': query = 'q=' + '+'.join(keyword_list)
        else: query = 'q=' + '+|+'.join(keyword_list)  
        params.append(query)
        completeurl = urlcreator('https', self.__api_url, r'search', params)
        print(completeurl)
        msg = backoff(completeurl, self.__attemps)
        response = json.loads(msg)
        data= {} # objeto que contendra la 'data' y el 'paging'
        data['data'] = []
        
        if 'data' in response:
            x = 0
            for i in response['data']:
                data['data'].append({})
                data['data'][x]['id'] = i['id']
                data['data'][x]['from'] = i['from']['id']
                data['data'][x]['from_name'] = i['from']['name']
                data['data'][x]['type'] = i['type']
                data['data'][x]['time'] = i['created_time']
                data['data'][x]['link'] = r'http://www.facebook.com/%s/posts/%s' % (i['id'].split('_')[0], i['id'].split('_')[1])
                
                if 'comments' in i:
                    data['data'][x]['comments'] = len(i['comments']['data'])
                else:
                    data['data'][x]['comments'] = 0
                                
                if 'likes' in i:

                    data['data'][x]['likes'] = len(i['likes']['data'])
                else:
                    data['data'][x]['likes'] = 0
                
                if 'shares' in i:
                    data['data'][x]['shares'] = i['shares']['count']
                else:
                    data['data'][x]['shares'] = 0
                    
                if 'message' in i:
                    data['data'][x]['message'] = i['message']
                else:
                    if i['type'] == 'photo':
                        data['data'][x]['message'] = i['caption'] if 'caption' in i else i['name']
                    elif i['type'] == 'link' or i['type'] == 'video':
                        if 'name' in i:
                            data['data'][x]['message'] = i['name']
                        else:
                            data['data'][x]['message'] = i['type']
                    elif i['type'] == 'question':
                        data['data'][x]['message'] = i['story']
                    else:
                        data['data'][x]['message'] = i['type']
                
                if 'message_tags' in i:
                    data['data'][x]['message_tags'] = i['message_tags']
                else:
                    data['data'][x]['message_tags'] = ''
                
                x += 1
                
        if 'paging' in response: # There's more pages for this request?
            data['paging'] = response['paging']
        else:
            data['paging'] = {}
        return data

if __name__ == '__main__':
    acces_token = r"293167064069597|D_XAWchMl1H8rLHjgKvDVwwZCig"
    api = Api(acces_token)
    data = api.parse_publication_by_id("188846691155660_10152118521199660")
    data = api.search_post("telefonica")
    #print(data['comments'])
    #print(data['likes'])
    for d in data['data']:
        print(d['comments'])
        print(d['likes'])
        print('\n')
