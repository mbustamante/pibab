# -*- coding: utf-8 -*-
'''
Created on Feb 23, 2012
@author: Roque Lopez
@contact: rlopezc27@gmail.com
'''
import threading
import copy
import config.data_base as db
from time import sleep
from crawler_facebook.facebook_api import Api
from analysis.classifier import Classifier
from config.utils import getUrlParameters, get_facebook_datetime, normalize_string, remove_whitespaces
from config.settings import TYPE_COMMENT, TYPE_PUBLICATION, COMENTARIO_FACEBOOK
from config.insert_relationship import InsertRelationshipKeyword, InsertRelationshipPage

FACEBOOK_ID = db.stored_procedure(db.STP_SEL_ID_FUENTE,
                              'Facebook')[0].id_Fuente                             
IDIOMS = dict((row.ds_Tipo, row.num_Tipo)
                 for row in db.stored_procedure(db.STP_SEL_TP_IDIOMA))

class FB_Thread(threading.Thread):
    ''' Clase que representa un hilo '''
    
    def __init__(self, id_name, request):
        threading.Thread.__init__(self)
        self.acces_token = request.get_access_token()
        self.request = request
        self.id_name = id_name
        self.api = Api(self.acces_token)
        self.classifier = Classifier()
        self.client_list = dict()
        self.until_date = None
    
    def run(self):
        print('info', self.id_name+": crawling old comments...")
        while True:
            self.crawling()
                       
    def sleeping(self):
        print('info', self.id_name+": sleeping", self.request.get_interval_crawling(), "seconds")        
        sleep(self.request.get_interval_crawling())
    
    def update_request(self, data, key_page=None):
        ''' Actualiza los datos de los requests '''
        self.request.set_start_date(data.fch_ini)
        self.request.set_end_date(data.fch_fin)
        self.request.set_interval_crawling(data.hora_req * 3600 + data.min_req * 60)
        # Actualizamos los datos de la lista
        self.update_list(key_page)
                                   
    def create_page(self, user_id, user_name, save_user):
        ''' Crea un usuario en la base de datos'''
        #if save_user: user = self.api.parse_user(user_id)
        #else: user = {}
        user = {}
        location = None
        followers = 0
        if 'username' in user: username = user['username']
        else: username = user_id
        if 'likes' in user: followers = user['likes']

        if 'locale' in user: language = IDIOMS.get(user['locale'])
        else: language= None
        
        data = db.stored_procedure(db.STP_INS_PAGINA,
                                username,
                                user_id,
                                FACEBOOK_ID,
                                language,
                                location,
                                followers,
                                None, # siguiendo
                                user_name)[0]
        return data.id_pagina_rpta
           
    def is_valid(self, text):
        ''' Verifica si un comentario es valido '''
        if text == 'status':
            return False
        if len(text) > 630:
            return False
        return True
    
class KeywordThread(FB_Thread):
    ''' Clase encargada de crawlear keywords '''
    
    def __init__(self, fb_keyword, bd_id_keyword, request, until_date, key_page):
        self.__fb_keyword = normalize_string(fb_keyword)
        self.__bd_id_keyword = bd_id_keyword
        super().__init__('key_'+str(fb_keyword), request)
        self.until_date = until_date
        self.__key_page_craw = None
        self.update_list(key_page)
    
    def update_list(self, key_page=None):
        ''' Actualiza la lista de clientes que crawlean el keyword '''
        if key_page:
            self.__key_page_craw = db.stored_procedure(db.STP_SEL_IDPAGINA_X_CODIGO, self.__fb_keyword, FACEBOOK_ID)[0].id_Pagina     
        
        for data in db.stored_procedure(db.STP_SEL_REL_X_KEYWORD, self.__bd_id_keyword, FACEBOOK_ID):
            idrk = data.id_RelKeyword
            if data.yn_activo:
                if idrk in self.client_list.keys():
                    self.client_list[idrk].update_client(data.valor_alerta, data.tp_opcion)
                else:
                    self.client_list[idrk] = Keyword_Client(data.valor_alerta, idrk, data.tp_opcion)
            else:    
                if idrk in self.client_list.keys():
                    self.client_list.pop(idrk)
              
    def crawling(self):
        ''' Crawlea los keywords '''
        result = self.api.search_post(self.__fb_keyword)
        data = result['data'] 
        self.save_batch_comment(data)
        paging = result['paging']
        
        if paging != {}:
            first_paging = copy.deepcopy(paging)
            while True:
                if 'next' in paging:
                    until_value = getUrlParameters(r''+paging['next'])['until']
                    if self.until_date >= int(until_value): break
                    result = self.api.search_post(self.__fb_keyword, until=until_value)
                    data = result['data'] 
                    self.save_batch_comment(data)
                    paging = result['paging']
                else:
                    break
            # Esperar que haya nuevos comentarios
            while True:
                print('info', self.id_name+": crawling news comments...")
                while True:
                    since_value = getUrlParameters(first_paging['previous'])['since']
                    result = self.api.search_post(self.__fb_keyword, since=since_value)
                    data = result['data'] 
                    self.save_batch_comment(data)
                    paging = result['paging']
                    if paging != {}:
                        first_paging = copy.deepcopy(paging)
                    else:
                        break
                self.sleeping()
        self.sleeping() 
                
    def save_batch_comment(self, data_list):
        ''' Almacena los comentarios en la base de datos '''
        for data in data_list:
            if self.is_valid(data['message']):
                text = remove_whitespaces(data['message'])
                self.classifier.classify(text, '1')
                first = True
                print('info', 'Almacenando o actualizando comentario con id %s' % data['id'])
                for client in self.client_list.values():
                    if self.without_exclusion(text, client.get_key_exclusion_list()):
                        if  first: 
                            row = db.stored_procedure(db.STP_INS_COMENTARIO,
                            data['id'],
                            text,
                            TYPE_PUBLICATION,
                            get_facebook_datetime(data['time']),
                            data['likes'],
                            data['shares'], # nro reteewt
                            data['comments'], # nro reply
                            None, # Comentario padre
                            self.__key_page_craw,
                            self.create_page(data['from'], data['from_name'], self.request.get_save_user()),
                            0, # hashtag
                            self.classifier.get_contribution_positive(),
                            self.classifier.get_contribution_negative(),
                            self.classifier.get_score(),
                            COMENTARIO_FACEBOOK,
                            data['link'],
                            )[0]
                            id_comment, created = row.id_comentario_rpta, row.yn_nuevo
                            first = False
                        if client.get_alert_value():
                            client.get_obj_insert().verify_by_keyword(id_comment, text, data['link'])
                        print('info', 'Enlazando keyword %s con comentario %s' % (self.__bd_id_keyword, id_comment))
                        client.get_obj_insert().insert_all_by_keyword(id_comment, text)
    
    def without_exclusion(self, comment, exclusion_list):
        ''' Verifica si un comentario contiene algun keyword de exclusion '''
        comment = normalize_string(comment.lower())
        for key in exclusion_list.values():
            if comment.count(key) > 0:
                return False              
        return True                   
            
class PageThread(FB_Thread):
    ''' Clase encargada de crawlear paginas '''
    
    def __init__(self, fb_page, bd_id_page, request, until_date):
        self.__fb_page = fb_page
        self.__bd_id_page = bd_id_page
        super().__init__('page_'+str(fb_page), request)
        self.id_user = self.api.parse_user(self.__fb_page)['id']
        self.until_date = until_date      
        self.update_list()
        
    def update_list(self, key_page=None):
        ''' Actualiza la lista de clientes que crawlean la pagina '''
        for data in db.stored_procedure(db.STP_SEL_REL_X_PAGINA, self.__bd_id_page):
            idcp = data.id_cliente_pagina
            if data.yn_activo:
                if idcp in self.client_list.keys():
                    self.client_list[idcp].update_client(data.valor_alerta, data.id_grupo)
                else:
                    self.client_list[idcp] = Page_Client(data.valor_alerta, idcp, data.id_grupo)
            else:    
                if idcp in self.client_list.keys():
                    self.client_list.pop(idcp)
                                
    def crawling(self):   
        ''' Crawlea las paginas '''
        result = self.api.parse_publication(self.__fb_page)
        data = result['data'] 
        self.__analize_publication(data)
        paging = result['paging']
        
        if paging != {}:
            first_paging = copy.deepcopy(paging)
            while True:
                if 'next' in paging:
                    until_value = getUrlParameters(r''+paging['next'])['until']
                    if self.until_date >= int(until_value): break
                    result = self.api.parse_publication(self.__fb_page, until=until_value)
                    data = result['data'] 
                    self.__analize_publication(data)
                    paging = result['paging']
                else:
                    break         
            # Esperar que haya nuevos comentarios
            while True:
                print('info', self.id_name+": crawling news comments...")
                while True:
                    since_value = getUrlParameters(first_paging['previous'])['since']
                    result = self.api.parse_publication(self.__fb_page, since=since_value)
                    data = result['data'] 
                    self.__analize_publication(data)
                    paging = result['paging']
                    if paging != {}:
                        first_paging = copy.deepcopy(paging)
                    else:
                        break
                self.sleeping() 
                self.__update_post()
                self.sleeping()        
        self.sleeping()
        
    def __update_post(self):
        ''' Actualiza los datos de una publicacion'''
        print('info', self.id_name+": updating posts...")
        posts = db.stored_procedure(db.STP_SEL_COMENTARIOS_X_ACTUALIZAR, self.__bd_id_page)
        for post in posts:
            data = self.api.parse_publication_by_id(post.id)
            db.stored_procedure(db.STP_UPD_POST, post.id_comentario, data['likes'], data['comments'])
            self.__analize_comment(post.id, post.id_comentario)
               
    def __analize_publication(self, data):
        ''' Recorre cada una de las publicaciones '''
        for publication in data:
            id_comment = self.save_publication(publication)
            if publication['comments'] > 0 and id_comment: # Hay comentarios de la publicacion?
                self.__analize_comment(publication['id'], id_comment)
                        
    def __analize_comment(self, id_publication, id_parent_comment):
        ''' Obtiene todo los comentarios de una publicacion '''
        result = self.api.parse_comment(id_publication)
        data = result['data'] 
        self.save_batch_comment(data, id_parent_comment)
        
        paging = result['paging']
        if paging != {}:
            while True:
                if 'next' in paging:
                    #print(r''+paging['next'])
                    after_id_value = getUrlParameters(paging['next'])['__after_id']
                    result = self.api.parse_comment(id_publication, after_id=after_id_value)
                    data = result['data'] 
                    self.save_batch_comment(data, id_parent_comment)
                    paging = result['paging']
                else:
                    return
                
    def save_publication(self, data):
        ''' Almacena una publicacion en la base de datos '''
        if not self.is_valid(data['message']): return None
        print('info', 'Almacenando o actualizando comentario con id %s' % data['id'])
        text = remove_whitespaces(data['message'])
        date_comment = get_facebook_datetime(data['time'])
        self.classifier.classify(text, '1')
        id_page = self.create_page(data['from'], data['from_name'], self.request.get_save_user())
        row = db.stored_procedure(db.STP_INS_COMENTARIO,
                data['id'],
                text,
                TYPE_PUBLICATION,
                date_comment,
                data['likes'],
                data['shares'], # nro reteewt
                data['comments'], # nro reply
                None, # Comentario padre
                self.__bd_id_page,
                id_page,
                0, # hashtag
                self.classifier.get_contribution_positive(),
                self.classifier.get_contribution_negative(),
                self.classifier.get_score(),
                COMENTARIO_FACEBOOK,
                data['link']
                )[0]
        id_comment, created = row.id_comentario_rpta, row.yn_nuevo

        for client in self.client_list.values():
            if client.get_alert_value() and self.id_user == data['from']: 
                client.get_obj_insert().verify_by_page(id_comment, text, data['link'], self.__bd_id_page)
            client.get_obj_insert().verify_by_influential(id_comment, text, data['link'], id_page)
            print('info', 'Enlazando pagina %s con comentario %s' % (self.__bd_id_page, id_comment))
            client.get_obj_insert().insert_all_by_page(id_comment, text)

        return id_comment
                   
    def save_batch_comment(self, data_list, parent):
        ''' Almacena los comentarios de una publicacion en la base de datos '''
        for data in data_list:
            if self.is_valid(data['message']):
                print('info', 'Almacenando o actualizando comentario con id %s' % data['id'])
                text = remove_whitespaces(data['message'])
                self.classifier.classify(text, '1')
                id_page = self.create_page(data['from'], data['from_name'], self.request.get_save_user())
                row = db.stored_procedure(db.STP_INS_COMENTARIO,
                        data['id'],
                        text,
                        TYPE_COMMENT,
                        get_facebook_datetime(data['time']),
                        data['likes'],
                        None, # nro reteewt
                        data['comments'], # nro reply
                        parent, # Comentario padre
                        self.__bd_id_page,
                        id_page,
                        0, # hashtag
                        self.classifier.get_contribution_positive(),
                        self.classifier.get_contribution_negative(),
                        self.classifier.get_score(),
                        COMENTARIO_FACEBOOK,
                        data['link']
                        )[0]
                id_comment, created = row.id_comentario_rpta, row.yn_nuevo               
                for client in self.client_list.values():
                    if client.get_alert_value() and self.id_user == data['from']:    
                        client.get_obj_insert().verify_by_page(id_comment, text, data['link'], self.__bd_id_page)
                    client.get_obj_insert().verify_by_influential(id_comment, text, data['link'], id_page)
                    print('info', 'Enlazando pagina %s con comentario %s' % (self.__bd_id_page, id_comment))
                    client.get_obj_insert().insert_all_by_page(id_comment, text)


class Keyword_Client(object):
    
    def __init__(self, alert_value, id_relkeyword, tp_option):
        self.__alert_value =  alert_value
        self.__id_relkeyword = id_relkeyword
        self.__id_client = db.stored_procedure(db.STP_SEL_CLIENTE_X_KEYWORD, id_relkeyword, int(alert_value))[0].id_cliente
        self.__obj_insert = InsertRelationshipKeyword(self.__id_client, id_relkeyword, tp_option)
        self.__key_exclusion_list = dict()
        self.__update_list(tp_option)
        
    def get_alert_value(self):
        return self.__alert_value
    
    def get_obj_insert(self):
        return self.__obj_insert
    
    def get_key_exclusion_list(self):
        return self.__key_exclusion_list
    
    def update_client(self, alert_value, tp_option):
        self.__alert_value = alert_value
        self.__obj_insert.update()
        self.__update_list(tp_option)

    def __update_list(self, tp_option):
        for data in db.stored_procedure(db.STP_SEL_KEYWORD_EXCLUSION, self.__id_client, self.__id_relkeyword, tp_option):
            idrk = data.id_RelKeyword
            if data.yn_activo:
                if not idrk in self.__key_exclusion_list.keys():
                    self.__key_exclusion_list[idrk] = data.ds_lema
            else:    
                if idrk in self.__key_exclusion_list.keys():
                    self.__key_exclusion_list.pop(idrk)
        
class Page_Client(object):
    
    def __init__(self, alert_value, id_relpage, id_group):
        self.__alert_value =  alert_value
        self.__obj_insert = InsertRelationshipPage(db.stored_procedure(db.STP_SEL_CLIENTE_X_PAGINA, id_relpage)[0].id_cliente, id_group)
    
    def get_alert_value(self):
        return self.__alert_value
    
    def get_obj_insert(self):
        return self.__obj_insert
    
    def update_client(self, alert_value, is_my_page):
        self.__alert_value = alert_value
        self.__obj_insert.update(is_my_page)
