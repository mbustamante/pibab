# -*- coding: utf-8 -*-
'''
Created on Feb 23, 2012
@author: eLRuLL, Roque Lopez
@contact: rlopezc27@gmail.com
# Aqui estan funciones relacionadas a errores que puedan ocurrir en cualquier parte del codigo.
'''
from urllib.request import urlopen
from time import sleep

def backoff(_url, attemps=10):
    ''' Controla el nro de intentos en caso haya problemas con el internet '''
    i = 0
    msg = None
    while i < attemps:
        print("dentro")
        try:
            response_c = urlopen(_url)
            msg = str(response_c.read(),'utf-8')
            break
        except Exception as e: # si sucede algun error, duerme exponencialmente.
            print(e)
            print("ERROR CONEXION INTERNET", _url)
            sleep(15)
        i += 1
    return msg

class AttempException(Exception):
    pass