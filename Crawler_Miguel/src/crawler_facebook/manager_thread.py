# -*- coding: utf-8 -*-
'''
Created on 19/04/2012
@author: Roque Lopez
@contact: rlopezc27@gmail.com
'''
import threading
import config.data_base as db
from time import sleep, time
from datetime import datetime, timedelta
from crawler_facebook.facebook_thread import KeywordThread, PageThread
from config.utils import timetounixtime
from config.settings import CUENTA_FACEBOOK


class Manager(threading.Thread):
    ''' Clase encargada  del control y gestion de los hilos '''

    def __init__(self, interval=5): 
        threading.Thread.__init__(self)
        self.__keyword_list = dict()
        self.__page_list = dict()
        self.__update_interval = interval * 60
        print('info', 'Iniciando el Crawler')
    
    def run(self):
        start_time = time() - self.__update_interval
        while True:
            if time() - start_time >= self.__update_interval:
                start_time = time()
                self.launch()
            sleep(15)
        
    def launch(self):
        ''' Inicia o actualiza los hilos  si estan activos '''       
        
        now = datetime.now()
        print('info', 'Sincronizando Keywords ...')
        # creando o eliminando los hilos de keywords  
        data_keyword_list = db.stored_procedure(db.STP_SEL_REQUEST_KEYWORD, CUENTA_FACEBOOK)
        for data in data_keyword_list:
            id_keyword = data.id_keyword
            if data.yn_activo:
                if  not id_keyword in self.__keyword_list.keys():
                    until_date = timetounixtime(datetime(now.year, now.month, now.day) - timedelta(days=data.dias_atras))
                    self.__keyword_list[id_keyword] = KeywordThread(data.ds_lema, id_keyword, Request(data), until_date, data.pagina_keyword)
            else:
                if id_keyword in self.__keyword_list.keys():
                    self.kill_thread(self.__keyword_list, id_keyword)          
                        
        print('info', 'Sincronizando Paginas ...')
        # creando o eliminando los hilos de paginas  
        data_page_list = db.stored_procedure(db.STP_SEL_REQUEST_PAGINA, CUENTA_FACEBOOK)
        for data in data_page_list:
            id_page = data.id_pagina
            if data.yn_activo:
                if  not id_page in self.__page_list.keys():
                    until_date = timetounixtime(datetime(now.year, now.month, now.day) - timedelta(days=data.dias_atras))
                    self.__page_list[id_page] = PageThread(data.ds_pagina, id_page, Request(data), until_date)
            else:
                if id_page in self.__keyword_list.keys():
                    self.kill_thread(self.__page_list, id_page)
        
        # lanzado o actualizando los hilos de keywords    
        for data in data_keyword_list:
            id_keyword = data.id_keyword
            if  id_keyword in self.__keyword_list.keys():
                if self.__keyword_list[id_keyword].is_alive():
                    self.__keyword_list[id_keyword].update_request(data, data.pagina_keyword)
                else:
                    self.__keyword_list[id_keyword].start()
        # lanzado o actualizando los hilos de paginas  
        for data in data_page_list:
            id_page = data.id_pagina
            if  id_page in self.__page_list.keys():
                if self.__page_list[id_page].is_alive():
                    self.__page_list[id_page].update_request(data)
                else:
                    self.__page_list[id_page].start()
                   
        print('info', 'Terminada la sincronizacion con la base de datos.')             
                 
               
    def kill_thread(self, threads, id_thread):
        ''' Mata un hilo, dado su id '''
        threads[id_thread].join()
        del threads[id_thread]
            
  
class Request:
    ''' Clase  que contiene informacion sobre los request'''
    
    def __init__(self, data):
        self.__acces_token = data.access_token
        self.__end_date = data.fch_fin
        self.__start_date = data.fch_ini
        self.__save_parent = data.craw_padre
        self.__save_user = data.craw_usuario
        self.__interval = data.hora_req * 3600 + data.min_req * 60
        
    def get_access_token(self):
        return self.__acces_token
    
    def get_start_date(self):
        return self.__start_date
       
    def get_end_date(self):
        return self.__end_date
    
    def get_interval_crawling(self):
        return self.__interval
    
    def get_save_user(self):
        return self.__save_user
    
    def get_save_parent(self):
        return self.__save_parent
       
    def set_access_token(self, access):
        self.__acces_token = access
    
    def set_start_date(self, start_date):
        self.__start_date = start_date
       
    def set_end_date(self, end_date):
        self.__end_date = end_date
    
    def set_interval_crawling(self, interval):
        self.__interval = interval
                